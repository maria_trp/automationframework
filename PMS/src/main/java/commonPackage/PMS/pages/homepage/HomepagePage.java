package commonPackage.PMS.pages.homepage;

import PMS.elementrepository.homepage.HomepagePageObjects;
import commonPackage.PMS.jsonbin.PMSData;
import commonPackage.PMS.pages.PMSBasePage;
import commonPackage.tools.Generator;
import commonPackage.tools.JsonDataLoader;
import commonPackage.tools.TimingsManager;
import org.openqa.selenium.WebDriver;

public class HomepagePage extends PMSBasePage {

    public static PMSData currentPMSData;

    public HomepagePage(WebDriver driver, TimingsManager timings) throws Exception {
        super(driver, timings);
        waitUntilPageisCompleted();
    }

    public void fillInSignUpForm(String signUpData) throws Exception {
        currentPMSData = JsonDataLoader.loadFromClassJson(signUpData, PMSData.class);
        String randomSignUpEmail = Generator.randomText()+"@gmail.com";

        type(HomepagePageObjects.FIRSTNAME, currentPMSData.getFirstname());
        type(HomepagePageObjects.LASTNAME, currentPMSData.getLastname());
        type(HomepagePageObjects.NEW_USERNAME, Generator.randomText());
        type(HomepagePageObjects.NEW_EMAIL, randomSignUpEmail);
        type(HomepagePageObjects.NEW_PASSWORD, currentPMSData.getNewPassword());
        type(HomepagePageObjects.CONFIRM_PASSWORD, currentPMSData.getNewPassword());

        clickAcceptTermsOfUseButton();
        clickSignUpButton();
    }

    public void clickAcceptTermsOfUseButton() throws Exception {
        clickButton(HomepagePageObjects.ACCEPT_TERMS_OF_USE_BUTTON);
    }

    public void clickNewSignUpButton() throws Exception {
        clickButton(HomepagePageObjects.NEW_SIGN_UP_BUTTON);
    }

}