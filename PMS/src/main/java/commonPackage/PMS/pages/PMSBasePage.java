package commonPackage.PMS.pages;

import PMS.elementrepository.dashboard.DashboardPageObjects;
import PMS.elementrepository.homepage.HomepagePageObjects;
import commonPackage.PMS.jsonbin.PMSData;
import commonPackage.PMS.pages.dashboard.DashboardPage;
import commonPackage.PMS.pages.propertyListing.PropertyListingPage;
import commonPackage.pages.PageBase;
import commonPackage.tools.JsonDataLoader;
import commonPackage.tools.TimingsManager;
import org.openqa.selenium.WebDriver;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PMSBasePage extends PageBase {

    public static PMSData currentPMSData;


    public PMSBasePage(WebDriver driver) throws Exception {
        super(driver);
        waitUntilPageisCompleted();
    }

    public PMSBasePage(WebDriver driver, TimingsManager timings) throws Exception {
        super(driver, timings);
        waitUntilPageisCompleted();

    }

    public static String testDateFormatter (){
        final LocalDateTime localtimestamp = LocalDateTime.now();
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.SSS");
        return localtimestamp.format(formatter);
    }

    public void insertAccountDetails(String data) throws Exception {
        currentPMSData = JsonDataLoader.loadFromClassJson(data, PMSData.class);
        //clickButton(ACCEPT_COOKIES);
        type(HomepagePageObjects.USERNAME_INPUT, currentPMSData.getUsername());
        type(HomepagePageObjects.PASSWORD_INPUT, currentPMSData.getPassword());
    }

    public DashboardPage clickSignInButton() throws Exception {
        clickButton(HomepagePageObjects.SIGN_IN_BUTTON);

        return new DashboardPage(driver, timings);
    }

    public String getIncorrectLoginDataMessage() throws Exception {
        return getMessage(HomepagePageObjects.LOGIN_ERROR_MESSAGE);
    }

    public void clickSignUpButton() throws Exception {
        clickButton(HomepagePageObjects.SIGN_UP_BUTTON);
    }
    public PropertyListingPage clickListYourPropertyButton() throws Exception {
        clickButton(DashboardPageObjects.LIST_YOUR_PROPERTY_BUTTON);

        return new PropertyListingPage(driver, timings);
    }
}
