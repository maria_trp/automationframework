package commonPackage.PMS.pages.myListings;

import PMS.elementrepository.myListings.MyListingsPageObjects;
import commonPackage.PMS.jsonbin.PMSData;
import commonPackage.PMS.pages.PMSBasePage;
import commonPackage.tools.JsonDataLoader;
import commonPackage.tools.TimingsManager;
import org.openqa.selenium.WebDriver;

public class MyListingsPage extends PMSBasePage {

    //constructor
    public MyListingsPage(WebDriver driver, TimingsManager timings) throws Exception {
        super(driver, timings);
        waitUntilPageisCompleted();
    }

    public void searchPropertyListing(String propertySearchData) throws Exception {
        PMSData propertyDetailsToSearch = JsonDataLoader.loadFromClassJson(propertySearchData, PMSData.class);
        type(MyListingsPageObjects.SEARCH_BAR, propertyDetailsToSearch.getPropertyListingToSearch());
    }

    public String getSearchResult() throws Exception {
        return getMessage(MyListingsPageObjects.SEARCH_RESULT);
    }

}

