package commonPackage.PMS.pages.reporting;

import commonPackage.PMS.pages.PMSBasePage;
import commonPackage.tools.TimingsManager;
import org.openqa.selenium.WebDriver;

public class ReportingPage extends PMSBasePage {

    public ReportingPage(WebDriver driver, TimingsManager timings) throws Exception {
        super(driver, timings);
        waitUntilPageisCompleted();

    }

}