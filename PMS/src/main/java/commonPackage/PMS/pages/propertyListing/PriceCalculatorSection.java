package commonPackage.PMS.pages.propertyListing;

import PMS.elementrepository.propertyListing.PriceCalculatorSectionPageObjects;
import PMS.elementrepository.propertyListing.PropertyListingPageObjects;
import commonPackage.PMS.jsonbin.PMSData;
import commonPackage.PMS.pages.PMSBasePage;
import commonPackage.tools.Generator;
import commonPackage.tools.JsonDataLoader;
import commonPackage.tools.TimingsManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.net.URL;

public class PriceCalculatorSection extends PMSBasePage {

    public static PMSData propertyDetailsData;

    public PriceCalculatorSection(WebDriver driver, TimingsManager timings) throws Exception {
        super(driver, timings);
        waitUntilPageisCompleted();
    }

    public void fillInPropertyNameTypeDetails(String propertyDetails) throws Exception {
        propertyDetailsData = JsonDataLoader.loadFromClassJson(propertyDetails, PMSData.class);
        type(PropertyListingPageObjects.PROPERTY_NAME_INPUT, "Random Listing " + Generator.randomText());
        selectExactMatchDropDownList(PropertyListingPageObjects.SELECT_TYPE_BUTTON, PropertyListingPageObjects.SELECT_TYPE_LIST, propertyDetailsData.getPropertyType());
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_PROPERTY_TYPE);
    }

    public void selectDatesForTwoWeeksNextTwoMonthsFromSideCalendar() throws Exception {
        clickButton(PriceCalculatorSectionPageObjects.SELECT_DATES_INPUT);
        clickButton(PriceCalculatorSectionPageObjects.NEXT_MONTH_BUTTON);
        clickButton(PriceCalculatorSectionPageObjects.NEXT_MONTH_BUTTON);
        clickButton(PriceCalculatorSectionPageObjects.TOP_RIGHT_DATE);
        clickButton(PriceCalculatorSectionPageObjects.TWO_WEEKS_AFTER_START_DATE);
    }

    public void insertPriceCalculatorDetails(String priceCalculatorData) throws Exception {
        propertyDetailsData = JsonDataLoader.loadFromClassJson(priceCalculatorData, PMSData.class);
        PriceCalculatorSection priceCalculatorSection = new PriceCalculatorSection(driver, timings);
        priceCalculatorSection.selectDatesForTwoWeeksNextTwoMonthsFromSideCalendar();
        clickButton(PriceCalculatorSectionPageObjects.GUESTS_INCREASE_BUTTON);
    }

    public void clickCalculateButton() throws Exception {
        clickButton(PriceCalculatorSectionPageObjects.CALCULATE_BUTTON);
    }

    public String verifyPricePerNight() throws Exception {
        return getMessage(PriceCalculatorSectionPageObjects.PRICE_PER_NIGHT_VALUE);
    }

    public String verifyDiscounts() throws Exception {
        return getMessage(PriceCalculatorSectionPageObjects.DISCOUNTS_VALUE);
    }

    public String verifyTotalPayout() throws Exception {
        return getMessage(PriceCalculatorSectionPageObjects.TOTAL_PAYOUT_VALUE);
    }

}
