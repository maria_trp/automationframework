package commonPackage.PMS.pages.propertyListing;

import PMS.elementrepository.propertyListing.PriceCalculatorSectionPageObjects;
import PMS.elementrepository.propertyListing.PropertyListingPageObjects;
import commonPackage.PMS.jsonbin.PMSData;
import commonPackage.PMS.pages.PMSBasePage;
import commonPackage.tools.Generator;
import commonPackage.tools.JsonDataLoader;
import commonPackage.tools.TimingsManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.net.URL;

public class PropertyListingPage extends PMSBasePage {

    public static PMSData propertyDetailsData;

    public PropertyListingPage(WebDriver driver, TimingsManager timings) throws Exception {
        super(driver, timings);
        waitUntilPageisCompleted();
    }

    public void fillInPropertyNameTypeDetails(String propertyDetails) throws Exception {
        propertyDetailsData = JsonDataLoader.loadFromClassJson(propertyDetails, PMSData.class);
        type(PropertyListingPageObjects.PROPERTY_NAME_INPUT, "Random Listing " + Generator.randomText());
        selectExactMatchDropDownList(PropertyListingPageObjects.SELECT_TYPE_BUTTON, PropertyListingPageObjects.SELECT_TYPE_LIST, propertyDetailsData.getPropertyType());
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_PROPERTY_TYPE);
    }

    public void fillInAccuratePropertyDetails(String propertyDetails) throws Exception {
        propertyDetailsData = JsonDataLoader.loadFromClassJson(propertyDetails, PMSData.class);
        type(PropertyListingPageObjects.PROPERTY_SIZE_INPUT, propertyDetailsData.getPropertySize());
        type(PropertyListingPageObjects.PROPERTY_ADDRESS_INPUT, propertyDetailsData.getPropertyAddress());
        driver.findElements(By.cssSelector(".pac-item")).get(0).click();
        clickButton(PropertyListingPageObjects.CONFIRM_PIN_BUTTON);
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_ACCURATE_PROPERTY_DETAILS);
    }

    public void fillInPropertyDescriptions(String propertyDetails) throws Exception {
        propertyDetailsData = JsonDataLoader.loadFromClassJson(propertyDetails, PMSData.class);
        type(PropertyListingPageObjects.SHORT_DESCRIPTION_INPUT, propertyDetailsData.getShortDescription());
        type(PropertyListingPageObjects.FULL_DESCRIPTION_INPUT, Generator.randomText() + "This is the long description required for the required data of the property listing " + Generator.randomText());
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_DESCRIPTION_DETAILS);
    }

    public void fillSleepingArrangementDetails() throws Exception {
        clickButton(PropertyListingPageObjects.INCREASE_GUESTS_BUTTON);
        clickButton(PropertyListingPageObjects.INCREASE_BEDROOMS_BUTTON);
        clickButton(PropertyListingPageObjects.INCREASE_BATHROOMS_BUTTON);
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_SLEEPING_ARRANGEMENTS);
        clickButton(PropertyListingPageObjects.INCREASE_BEDROOM_GUESTS_BUTTON);
        clickButton(PropertyListingPageObjects.INCREASE_DOUBLE_BED_BUTTON);
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_SLEEPING_ARRANGEMENTS);
    }

    public void fillInAmenitiesDetails() throws Exception {
        clickButton(PropertyListingPageObjects.FREE_WIFI_CHECKBOX);
        clickButton(PropertyListingPageObjects.TOILETRIES_CHECKBOX);
        clickButton(PropertyListingPageObjects.AC_CHECKBOX);
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_AMENITIES);
    }

    public void fillInHostReceptionDetails() throws Exception {
        clickButton(PropertyListingPageObjects.EMERGENCY_CONTACT_NUMBER);
        clickButton(PropertyListingPageObjects.TWENTY_FOUR_HOUR_CHECK_IN);
        clickButton(PropertyListingPageObjects.SELF_CHECKOUT);
        clickButton(PropertyListingPageObjects.COMPLEMENTARY_CLEANING);
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_HOST_RECEPTION);
    }

    public void fillInHouseRulesDetails(String propertyDetails) throws Exception {
        propertyDetailsData = JsonDataLoader.loadFromClassJson(propertyDetails, PMSData.class);
        clickButton(PropertyListingPageObjects.FLEXIBLE_CHECK_IN);
        clickButton(PropertyListingPageObjects.FLEXIBLE_CHECKOUT);
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_HOUSE_RULES);
        selectExactMatchDropDownList(PropertyListingPageObjects.SELECT_CHECK_IN_BUTTON, PropertyListingPageObjects.SELECT_CHECK_IN_DROPDOWN_LIST, propertyDetailsData.getCheckInTime());
        selectExactMatchDropDownList(PropertyListingPageObjects.SELECT_CHECKOUT_BUTTON, PropertyListingPageObjects.SELECT_CHECKOUT_DROPDOWN_LIST, propertyDetailsData.getCheckOutTime());
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_HOUSE_RULES);
    }

    public void UploadPhotosSection() throws Exception {
        //clickButton(PropertyListingPageObjects.UPLOAD_FILE_BUTTON);

        WebElement inputElement = driver.findElement(By.xpath(".//input[@type='file']"));
        String uploadFilePath = "C:\\Users\\m.terpou\\Desktop\\AutomationUIFramework\\PMS\\src\\main\\resources\\Uploads\\images\\photo1.png";
        String uploadFilePath2 = "C:\\Users\\m.terpou\\Desktop\\AutomationUIFramework\\PMS\\src\\main\\resources\\Uploads\\images\\photo2.png";
        String uploadFilePath3 = "C:\\Users\\m.terpou\\Desktop\\AutomationUIFramework\\PMS\\src\\main\\resources\\Uploads\\images\\photo3.png";
        String uploadFilePath4 = "C:\\Users\\m.terpou\\Desktop\\AutomationUIFramework\\PMS\\src\\main\\resources\\Uploads\\images\\photo4.png";
        String uploadFilePath5 = "C:\\Users\\m.terpou\\Desktop\\AutomationUIFramework\\PMS\\src\\main\\resources\\Uploads\\images\\photo5.png";
        String uploadFilePath6 = "C:\\Users\\m.terpou\\Desktop\\AutomationUIFramework\\PMS\\src\\main\\resources\\Uploads\\images\\photo6.png";
        inputElement.sendKeys(uploadFilePath + "\n " + uploadFilePath2 + "\n " + uploadFilePath3+ "\n " + uploadFilePath4+ "\n " + uploadFilePath5+ "\n " + uploadFilePath6);
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_PHOTOS);
    }

    private File getFileFromResources(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }
    }
    public void MultipleFileUploadInChromeUsingMultipleSendKeysMethod() {
        String file1 = getFileFromResources("images/photo1.jpg").getAbsolutePath();
        String file2 = getFileFromResources("images/photo2.jpg").getAbsolutePath();
        driver.switchTo().frame("iframeResult").findElement(By.id("files")).sendKeys(file1);
        driver.findElement(By.id("files")).sendKeys(file2);
    }

    public void MultipleFileUploadInChromeUsingNewLineChar() {

        String filesPathSeparatedWithNewLineChar = getFileFromResources("images/photo3.jpg").getAbsolutePath()+"\n"+getFileFromResources("images/photo1.jpg");
        driver.switchTo().frame("iframeResult").findElement(By.id("files")).sendKeys(filesPathSeparatedWithNewLineChar);
    }

    public void fillInPriceDetails(String propertyDetails) throws Exception {
        propertyDetailsData = JsonDataLoader.loadFromClassJson(propertyDetails, PMSData.class);
        type(PropertyListingPageObjects.PRICE_PER_NIGHT, propertyDetailsData.getPricePerNight());
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_PRICES);
    }

    public void fillPricePerNight(String pricePerNight) throws Exception {
        propertyDetailsData = JsonDataLoader.loadFromClassJson(pricePerNight, PMSData.class);
        type(PropertyListingPageObjects.PRICE_PER_NIGHT, propertyDetailsData.getPricePerNight());
    }

    public void fillMinimumStayWithDiscount(String minimumStayWithDiscount) throws Exception {
        propertyDetailsData = JsonDataLoader.loadFromClassJson(minimumStayWithDiscount, PMSData.class);
        clear(PropertyListingPageObjects.MINIMUM_STAY_LONG_TERM);
        type(PropertyListingPageObjects.MINIMUM_STAY_LONG_TERM, propertyDetailsData.getMinimumStayLongTerm());
        clear(PropertyListingPageObjects.DISCOUNT_LONG_TERM);
        type(PropertyListingPageObjects.DISCOUNT_LONG_TERM, propertyDetailsData.getDiscountLongTerm());
    }

    public void fillLastMinuteWithDiscount(String lastMinuteWithDiscount) throws Exception {
        propertyDetailsData = JsonDataLoader.loadFromClassJson(lastMinuteWithDiscount, PMSData.class);
        type(PropertyListingPageObjects.DAYS_PRIOR_TO_CHECK_IN_LAST_MINUTE, propertyDetailsData.getDaysPriorCheckInLastMinute());
        clear(PropertyListingPageObjects.DISCOUNT_LAST_MINUTE);
        type(PropertyListingPageObjects.DISCOUNT_LAST_MINUTE, propertyDetailsData.getDiscountLastMinute());
    }

    public void clickPricingNextButton() throws Exception {
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_PRICES);
    }

    public void clickPriceCalculatorButton() throws Exception {
        clickButton(PropertyListingPageObjects.PRICE_CALCULATOR_BUTTON);
    }

    public void selectRatesAndAvailability() throws Exception {
        clickButton(PropertyListingPageObjects.CALENDAR_TODAY_DATE);
        clickButton(PropertyListingPageObjects.THREE_NEXT_DAYS);
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_RATES_AND_AVAILABILITY);
    }

    public void selectFlexiblePolicy() throws Exception {
        clickButton(PropertyListingPageObjects.FLEXIBLE_POLICY_CHECKBOX);
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_POLICIES);
    }

    public void selectModeratePolicy() throws Exception {
        clickButton(PropertyListingPageObjects.MODERATE_POLICY_CHECKBOX);
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_POLICIES);
    }

    public void selectStrictPolicy() throws Exception {
        clickButton(PropertyListingPageObjects.STRICT_POLICY_CHECKBOX);
        clickButton(PropertyListingPageObjects.NEXT_BUTTON_POLICIES);
    }

    public String getSuccessfulListingMessage() throws Exception {
        return getMessage(PropertyListingPageObjects.CONGRATULATIONS_MESSAGE);
    }

}
