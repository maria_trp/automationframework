package commonPackage.PMS.pages.dashboard;

import PMS.elementrepository.dashboard.DashboardPageObjects;
import PMS.elementrepository.myListings.MyListingsPageObjects;
import commonPackage.PMS.jsonbin.PMSData;
import commonPackage.PMS.pages.PMSBasePage;
import commonPackage.PMS.pages.myListings.MyListingsPage;
import commonPackage.tools.JsonDataLoader;
import commonPackage.tools.TimingsManager;
import org.openqa.selenium.WebDriver;

public class DashboardPage extends PMSBasePage {

    public static PMSData currentPMSData;

    public DashboardPage(WebDriver driver, TimingsManager timings) throws Exception {
        super(driver, timings);
        waitUntilPageisCompleted();
    }

    public String getExpectedWelcomeMessageFromJSON(String data) {
        currentPMSData = JsonDataLoader.loadFromClassJson(data, PMSData.class);
        assert currentPMSData != null;

        return currentPMSData.getExpectedLoginMessage();
    }

    public String viewActualWelcomeMessage() throws Exception {
        return getMessage(DashboardPageObjects.WELCOME_MESSAGE);
    }

    public void clickLogoutButton() throws Exception {
        clickButton(DashboardPageObjects.USER_ICON);
        clickButton(DashboardPageObjects.LOGOUT_BUTTON);
    }

    public String verifySuccessfulSignUp() throws Exception {
        return getMessage(DashboardPageObjects.CHANGE_PROFILE_PHOTO_SECTION);
    }

    public MyListingsPage clickMyListingsMenuButton() throws Exception {
        clickButton(MyListingsPageObjects.SEARCH_BAR);

        return new MyListingsPage(driver, timings);
    }

}