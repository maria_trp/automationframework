package commonPackage.PMS.pages.transactions;

import commonPackage.tools.TimingsManager;
import commonPackage.PMS.pages.PMSBasePage;
import org.openqa.selenium.WebDriver;

public class TransactionsPage extends PMSBasePage {

    public TransactionsPage(WebDriver driver, TimingsManager timings) throws Exception {
        super(driver, timings);
        waitUntilPageisCompleted();

    }

}
