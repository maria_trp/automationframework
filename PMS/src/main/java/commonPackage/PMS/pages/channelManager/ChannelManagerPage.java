package commonPackage.PMS.pages.channelManager;

import commonPackage.PMS.pages.PMSBasePage;
import commonPackage.tools.TimingsManager;
import org.openqa.selenium.WebDriver;

public class ChannelManagerPage extends PMSBasePage {

    public ChannelManagerPage(WebDriver driver, TimingsManager timings) throws Exception {
        super(driver, timings);
        waitUntilPageisCompleted();

    }

}