package commonPackage.PMS.jsonbin;

public class PMSData {

    //Login Data
    private String username;
    private String password;
    private String expectedLoginMessage;

    //Sign up Data
    private String firstname;
    private String lastname;
    private String newUsername;
    private String yourEmail;
    private String newPassword;

    //Property Listing Data
    private String propertyType;
    private String propertySize;
    private String propertyLicenseNumber;
    private String propertyAddress;
    private String shortDescription;
    private String checkInTime;
    private String checkOutTime;
    private String pricePerNight;
    private String cleaningFee;
    private String securityDeposit;
    private String minimumStay;
    private String chargePerGuest;
    private String weeklyDiscount;
    private String monthlyDiscount;
    private String minimumStayLongTerm;
    private String discountLongTerm;
    private String daysPriorCheckInLastMinute;
    private String discountLastMinute;
    private String daysPriorCheckInEarlyBird;
    private String discountEarlyBird;
    private String successfulListingMessage;

    //Search data
    private String propertyListingToSearch;

    //Price Calculator data
    private String calculatorPricePerNight;
    private String calculatorDiscounts;
    private String calculatorsTotalPayout;


    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getExpectedLoginMessage() {
        return expectedLoginMessage;
    }
    public void setExpectedLoginMessage(String expectedLoginMessage) {
        this.expectedLoginMessage = expectedLoginMessage;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getNewUsername() {
        return newUsername;
    }

    public void setNewUsername(String newUsername) {
        this.newUsername = newUsername;
    }

    public String getYourEmail() {
        return yourEmail;
    }

    public void setYourEmail(String yourEmail) {
        this.yourEmail = yourEmail;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertySize() {
        return propertySize;
    }

    public void setPropertySize(String propertySize) {
        this.propertySize = propertySize;
    }

    public String getPropertyLicenseNumber() {
        return propertyLicenseNumber;
    }

    public void setPropertyLicenseNumber(String propertyLicenseNumber) {
        this.propertyLicenseNumber = propertyLicenseNumber;
    }

    public String getPropertyAddress() {
        return propertyAddress;
    }

    public void setPropertyAddress(String propertyAddress) {
        this.propertyAddress = propertyAddress;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    public String getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public String getPricePerNight() {
        return pricePerNight;
    }

    public void setPricePerNight(String pricePerNight) {
        this.pricePerNight = pricePerNight;
    }

    public String getCleaningFee() {
        return cleaningFee;
    }

    public void setCleaningFee(String cleaningFee) {
        this.cleaningFee = cleaningFee;
    }

    public String getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(String securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    public String getMinimumStay() {
        return minimumStay;
    }

    public void setMinimumStay(String minimumStay) {
        this.minimumStay = minimumStay;
    }

    public String getChargePerGuest() {
        return chargePerGuest;
    }

    public void setChargePerGuest(String chargePerGuest) {
        this.chargePerGuest = chargePerGuest;
    }

    public String getWeeklyDiscount() {
        return weeklyDiscount;
    }

    public void setWeeklyDiscount(String weeklyDiscount) {
        this.weeklyDiscount = weeklyDiscount;
    }

    public String getMonthlyDiscount() {
        return monthlyDiscount;
    }

    public void setMonthlyDiscount(String monthlyDiscount) {
        this.monthlyDiscount = monthlyDiscount;
    }

    public String getMinimumStayLongTerm() {
        return minimumStayLongTerm;
    }

    public void setMinimumStayLongTerm(String minimumStayLongTerm) {
        this.minimumStayLongTerm = minimumStayLongTerm;
    }

    public String getDiscountLongTerm() {
        return discountLongTerm;
    }

    public void setDiscountLongTerm(String discountLongTerm) {
        this.discountLongTerm = discountLongTerm;
    }

    public String getDaysPriorCheckInLastMinute() {
        return daysPriorCheckInLastMinute;
    }

    public void setDaysPriorCheckInLastMinute(String daysPriorCheckInLastMinute) {
        this.daysPriorCheckInLastMinute = daysPriorCheckInLastMinute;
    }

    public String getDiscountLastMinute() {
        return discountLastMinute;
    }

    public void setDiscountLastMinute(String discountLastMinute) {
        this.discountLastMinute = discountLastMinute;
    }

    public String getDaysPriorCheckInEarlyBird() {
        return daysPriorCheckInEarlyBird;
    }

    public void setDaysPriorCheckInEarlyBird(String daysPriorCheckInEarlyBird) {
        this.daysPriorCheckInEarlyBird = daysPriorCheckInEarlyBird;
    }

    public String getDiscountEarlyBird() {
        return discountEarlyBird;
    }

    public void setDiscountEarlyBird(String discountEarlyBird) {
        this.discountEarlyBird = discountEarlyBird;
    }

    public String getSuccessfulListingMessage() {
        return successfulListingMessage;
    }

    public void setSuccessfulListingMessage(String successfulListingMessage) {
        this.successfulListingMessage = successfulListingMessage;
    }

    public String getPropertyListingToSearch() {
        return propertyListingToSearch;
    }

    public void setPropertyListingToSearch(String propertyListingToSearch) {
        this.propertyListingToSearch = propertyListingToSearch;
    }

    public String getCalculatorPricePerNight() {
        return calculatorPricePerNight;
    }

    public void setCalculatorPricePerNight(String calculatorPricePerNight) {
        this.calculatorPricePerNight = calculatorPricePerNight;
    }

    public String getCalculatorDiscounts() {
        return calculatorDiscounts;
    }

    public void setCalculatorDiscounts(String calculatorDiscounts) {
        this.calculatorDiscounts = calculatorDiscounts;
    }

    public String getCalculatorsTotalPayout() {
        return calculatorsTotalPayout;
    }

    public void setCalculatorsTotalPayout(String calculatorsTotalPayout) {
        this.calculatorsTotalPayout = calculatorsTotalPayout;
    }
}
