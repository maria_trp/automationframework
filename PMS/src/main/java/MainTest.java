import commonPackage.services.DataProcessor;
import org.junit.runner.JUnitCore;
import specifications.RegressionSpec;
import specifications.SanitySpec;

public class MainTest {
    public static void main(String[] args) throws Exception {

        String testSuite;
        DataProcessor dataProcessor;
        String customerName = "";

        if (args.length == 0) {
            System.setProperty("environment.name", "BUILD");
            System.setProperty("customer.name", "CORE");
            testSuite = "SANITY";
        } else {
            String environmentName = args[0];
            System.setProperty("environment.name", environmentName);

            customerName = args[1];
            System.setProperty("customer.name", customerName);

            testSuite = args[2];

        }

        JUnitCore engine = new JUnitCore();
        if (testSuite.equals("REGRESSION")) {
            engine.run(RegressionSpec.class);
        } else {
            engine.run(SanitySpec.class);
        }

    }
}

