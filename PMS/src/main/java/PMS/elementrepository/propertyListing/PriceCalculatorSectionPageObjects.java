package PMS.elementrepository.propertyListing;

public class PriceCalculatorSectionPageObjects {

   public static final String GUESTS_INCREASE_BUTTON = ".//i[@class='pms-icon font-size-16 font-weight-bold pms-arrow-up']";
   public static final String CALCULATE_BUTTON = ".//button[contains(text(),'Calculate')]";

   public static final String SELECT_DATES_INPUT = ".//input[@id='input-calculator']";
   public static final String NEXT_MONTH_BUTTON = ".//table[@id='month-1-input-calculator']//thead//tr[@class='datepicker__month-caption']//th//span[@class='datepicker__month-button datepicker__month-button--next'][normalize-space()='>']";
   public static final String TOP_RIGHT_DATE = ".//body[1]/div[2]/div[1]/section[1]/div[2]/section[1]/div[1]/div[1]/div[2]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[7]";
   public static final String TWO_WEEKS_AFTER_START_DATE = ".//body[1]/div[2]/div[1]/section[1]/div[2]/section[1]/div[1]/div[1]/div[2]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[3]/td[7]";

   public static final String PRICE_PER_NIGHT_VALUE = ".//td[contains(text(),'€100 x 14 nights')]";
   public static final String DISCOUNTS_VALUE = ".//td[contains(text(),'€81')]";
   public static final String TOTAL_PAYOUT_VALUE = ".//td[contains(text(),'€1330')]";
}

