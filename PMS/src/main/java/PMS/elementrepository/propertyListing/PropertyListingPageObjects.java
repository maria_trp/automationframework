package PMS.elementrepository.propertyListing;

public class PropertyListingPageObjects {

    //Section 1 of Property Listing - Property Naming
    public static final String PROPERTY_NAME_INPUT = ".//input[@id='property_title']";
    public static final String SELECT_TYPE_BUTTON = ".//option[contains(text(),'Studio')]";
    public static final String SELECT_TYPE_LIST = ".//select";
    public static final String NEXT_BUTTON_PROPERTY_TYPE = "(//button[contains(text(),'Next')])[1]";

    //Section 2 of Property Listing - Property Address Details
    public static final String PROPERTY_SIZE_INPUT = ".//input[@id='property_size']";
    public static final String PROPERTY_LICENSE_NUMBER_INPUT = ".//input[@id='property_license_number']";
    public static final String PROPERTY_ADDRESS_INPUT = ".//input[@id='pac-input']";
    public static final String CONFIRM_PIN_BUTTON = ".//button[contains(text(),'Confirm Pin Location')]";
    public static final String NEXT_BUTTON_ACCURATE_PROPERTY_DETAILS = ".//span[contains(text(),'Next')]";

    //Section 3 of Property Listing - Descriptions
    public static final String SHORT_DESCRIPTION_INPUT = ".//textarea[@id='short_desc']";
    public static final String FULL_DESCRIPTION_INPUT = ".//textarea[@id='desc']";
    public static final String NEXT_BUTTON_DESCRIPTION_DETAILS = ".//span[contains(text(),'Next')]";

    //Section 4 of Property Listing - Property rooms details
    public static final String INCREASE_GUESTS_BUTTON = ".//div[contains(@class,'mt-5')]//div[1]//div[1]//span[3]//button[1]";
    public static final String INCREASE_BEDROOMS_BUTTON = ".//div[contains(@class,'px-5')]//div[2]//div[1]//span[3]//button[1]";
    public static final String INCREASE_BATHROOMS_BUTTON = ".//div[3]//div[1]//span[3]//button[1]";
    public static final String NEXT_BUTTON_SLEEPING_ARRANGEMENTS = ".//span[contains(text(),'Next')]";
    public static final String INCREASE_BEDROOM_GUESTS_BUTTON = ".//div[contains(@class,'col-7 col-xl-3 order-xl-0 mb-4')]//span[contains(@class,'input-group-btn input-group-append ml-0')]//button[contains(@type,'button')]";
    public static final String INCREASE_DOUBLE_BED_BUTTON = ".//body//div[@id='layout-wrapper']//div[contains(@class,'container-fluid')]//div[contains(@class,'container-fluid')]//div[1]//div[2]//div[1]//span[3]//button[1]";

    //Section 5 of Property Listing - Property Amenities
    public static final String FREE_WIFI_CHECKBOX = ".//input[@id='am_133']";
    public static final String TOILETRIES_CHECKBOX = ".//input[@id='am_134']";
    public static final String AC_CHECKBOX = ".//input[@id='am_131']";
    public static final String FRESH_SHEET_TOWELS = ".//input[@id='am_130']";
    public static final String FULLY_EQUIPPED_KITCHEN = ".//input[@id='am_132']";
    public static final String HAIR_DRIER = ".//input[@id='am_129']";

    public static final String WIFI_SUBJECT_TO_CHARGE = ".//input[@id='am_140']";
    public static final String WASHING_MACHINE = ".//input[@id='am_137']";
    public static final String IRON = ".//input[@id='am_138']";
    public static final String HANGERS = ".//input[@id='am_142']";
    public static final String CABLE_INTERNET = ".//input[@id='am_141']";
    public static final String DRIER = ".//input[@id='am_14']";
    public static final String SAFE = ".//input[@id='am_139']";
    public static final String NEXT_BUTTON_AMENITIES = ".//span[contains(text(),'Next')]";

    //Section 6 of Property Listing - Property Host Reception
    public static final String EMERGENCY_CONTACT_NUMBER = ".//input[@id='service_28']";
    public static final String TWENTY_FOUR_HOUR_CHECK_IN = ".//input[@id='service_32']";
    public static final String SELF_CHECKOUT = ".//input[@id='service_34']";
    public static final String COMPLEMENTARY_CLEANING = ".//input[@id='service_36']";
    public static final String MEET_AND_GREET = ".//input[@id='service_30']";
    public static final String SELF_CHECK_IN = ".//input[@id='service_33']";
    public static final String LUGGAGE_STORAGE = ".//input[@id='service_35']";
    public static final String NEXT_BUTTON_HOST_RECEPTION = ".//span[contains(text(),'Next')]";

    //Section 7 of Property Listing - Property House Rules
    public static final String SELECT_CHECK_IN_BUTTON = ".//option[contains(text(),'Select check in time')]";
    public static final String SELECT_CHECK_IN_DROPDOWN_LIST = ".//select[@id='check_in_after']";
    public static final String SELECT_CHECKOUT_BUTTON = ".//option[contains(text(),'Select check out time')]";
    public static final String SELECT_CHECKOUT_DROPDOWN_LIST = ".//select[@id='check_out_before']";
    public static final String FLEXIBLE_CHECK_IN = ".//input[@id='check_in_flexible']";
    public static final String FLEXIBLE_CHECKOUT = ".//input[@id='check_out_flexible']";
    public static final String NO_SMOKING = ".//input[@id='no_smoking']";
    public static final String NO_PETS = ".//input[@id='no_pets']";
    public static final String NO_SOCIAL_GATHERING = ".//input[@id='no_social_gathering']";
    public static final String NEXT_BUTTON_HOUSE_RULES = ".//span[contains(text(),'Next')]";

    //Section 8 of Property Listing - Property Photos
    public static final String UPLOAD_FILE_BUTTON = ".//input[@type='file']";
    public static final String NEXT_BUTTON_PHOTOS = ".//span[contains(text(),'Next')]";

    //Section 9 of Property Listing - Property Pricing
    public static final String PRICE_PER_NIGHT = ".//input[@id='nightly']";
    public static final String CLEANING_FEE = ".//input[@id='cleaning_fee']";
    public static final String SECURITY_DEPOSIT = ".//input[@id='security_deposit']";
    public static final String MINIMUM_STAY= ".//input[@id='minimum_stay']";
    public static final String CHARGE_PER_GUEST = ".//input[@id='extra_guest']";
    public static final String WEEKLY_DISCOUNT = ".//body/div[@id='layout-wrapper']/div[1]/section[1]/div[2]/section[1]/div[1]/div[1]/div[1]/div[9]/section[1]/input[1]";
    public static final String MONTHLY_DISCOUNT = ".//body/div[@id='layout-wrapper']/div[1]/section[1]/div[2]/section[1]/div[1]/div[1]/div[1]/div[10]/section[1]/input[1]";
    public static final String MINIMUM_STAY_LONG_TERM = ".//input[@id='long_stay_discount_min_stay']";
    public static final String DISCOUNT_LONG_TERM = ".//body/div[@id='layout-wrapper']/div[1]/section[1]/div[2]/section[1]/div[1]/div[1]/div[1]/div[11]/div[1]/div[2]/section[1]/input[1]";
    public static final String DAYS_PRIOR_TO_CHECK_IN_LAST_MINUTE = ".//input[@id='last_minute_discount_max_stay']";
    public static final String DISCOUNT_LAST_MINUTE = ".//body/div[@id='layout-wrapper']/div[1]/section[1]/div[2]/section[1]/div[1]/div[1]/div[1]/div[13]/div[1]/div[2]/section[1]/input[1]";
    public static final String DAYS_PRIOR_TO_CHECK_IN_EARLY_BIRD = ".//input[@id='early_bird_discount_min_stay']";
    public static final String DISCOUNT_EARLY_BIRD = ".//body/div[@id='layout-wrapper']/div[1]/section[1]/div[2]/section[1]/div[1]/div[1]/div[1]/div[14]/div[1]/div[2]/section[1]/input[1]";
    public static final String NEXT_BUTTON_PRICES = ".//span[contains(text(),'Next')]";

    //Section 10 of Property Listing - Rates and Availability
    public static final String INSTANT_BOOKING_CHECKBOX = ".//input[@id='instant']";
    public static final String PRICE_CALCULATOR_BUTTON = ".//button[contains(text(),'Price calculator')]";
    public static final String NEXT_BUTTON_RATES_AND_AVAILABILITY = ".//span[contains(text(),'Next')]";
    public static final String CALENDAR_TODAY_DATE = ".//div[@class='day today']";
    public static final String THREE_NEXT_DAYS = ".//div[@class='day today']/following::div[3]";
    public static final String ADD_NEW_RATE_BUTTON = ".//button[contains(text(),'Add new rate')]";

    //Section 11 of Property Listing - Policies
    public static final String FLEXIBLE_POLICY_CHECKBOX = ".//input[@id='flexible']";
    public static final String MODERATE_POLICY_CHECKBOX = ".//input[@id='moderate']";
    public static final String STRICT_POLICY_CHECKBOX = ".//input[@id='strict']";
    public static final String NEXT_BUTTON_POLICIES = ".//span[contains(text(),'Next')]";

    //Final screen of Property Listing - Successful Insertion
    public static final String CONGRATULATIONS_MESSAGE = ".//h2[contains(text(),'Congratulations!')]";
    public static final String EDIT_BUTTON = ".//a[contains(text(),'Edit')]";
    public static final String MY_LISTINGS_BUTTON = ".//a[contains(text(),'My listings')]";

}

