package PMS.elementrepository.homepage;

public class HomepagePageObjects {

    /*public static final String ACCEPT_COOKIES = ".//div[@class='cc-compliance']";
    public static final String USERNAME_INPUT = "test";
    public static final String PASSWORD_INPUT = ".//input[@id='password']";
    public static final String SIGN_IN_BUTTON = ".//div[@class='btn form-submit-btn']";
     */

    public static final String USERNAME_INPUT = ".//body/div[@id='main_content']/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/input[1]";
    public static final String PASSWORD_INPUT = ".//body/div[@id='main_content']/div[1]/div[1]/div[1]/div[1]/form[1]/div[2]/input[1]";
    public static final String SIGN_IN_BUTTON = ".//body/div[@id='main_content']/div[1]/div[1]/div[1]/div[1]/form[1]/div[5]/button[1]";
    public static final String ACCEPT_COOKIES = ".//div[@class='cc-compliance']";
    public static final String LOGIN_ERROR_MESSAGE = ".//span[contains(text(),'Either your username or password')]";

    public static final String SIGN_UP_BUTTON = "(//*[contains(text(),'Sign up now!')])[2]";

    public static final String FIRSTNAME = "(//input[@id='first_name'])[2]";
    public static final String LASTNAME = "(//input[@id='last_name'])[2]";
    public static final String NEW_USERNAME = "(//input[@id='user_name'])[2]";
    public static final String NEW_EMAIL = "(//input[@id='email'])[2]";
    public static final String NEW_PASSWORD = "(//input[@id='password'])[3]";
    public static final String CONFIRM_PASSWORD = "(//input[@id='password_repeat'])[2]\n";
    public static final String ACCEPT_TERMS_OF_USE_BUTTON = "(//input[@id='terms_check'])[2]";
    public static final String NEW_SIGN_UP_BUTTON = "(//button[@id='signupSubmitButton'])[2]";

}