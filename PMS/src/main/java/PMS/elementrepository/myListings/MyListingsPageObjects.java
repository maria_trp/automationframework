package PMS.elementrepository.myListings;

public class MyListingsPageObjects {

    public static final String SEARCH_BAR = ".//a[contains(text(),'My Listings')]";
    public static final String SEARCH_RESULT = ".//a[normalize-space()='Test Property to be rented']";

}
