package PMS.elementrepository.dashboard;

public class DashboardPageObjects {

    public static final String WELCOME_MESSAGE = ".//h2[contains(text(),'Hello')]";
    public static final String LOGOUT_BUTTON = ".//a[contains(text(),'Logout')]";
    public static final String USER_ICON = ".//button[@id='page-header-user-dropdown']";

    public static final String CHANGE_PROFILE_PHOTO_SECTION = ".//div[@class='upload-image-explanation']";
    public static final String LIST_YOUR_PROPERTY_BUTTON = ".//button[contains(text(),'List your property')]";

}