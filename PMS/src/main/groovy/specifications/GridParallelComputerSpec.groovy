package specifications


import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore
import org.junit.runner.Result
import org.junit.runners.Suite
import spock.lang.Specification

import static commonPackage.tools.GetSystemParameters.getNumberOfThreads
import static commonPackage.tools.GetSystemParameters.getRunTest

class GridParallelComputerSpec extends Specification {

    def "Parallel sanity test execution"() {
        when:
            String noThreads = getNumberOfThreads()
            String testToRun = getRunTest()
            int no = Integer.parseInt(noThreads)

            Class<?> clazz = Class.forName(testToRun)

            List<Class<?>> classes = clazz.getAnnotation(Suite.SuiteClasses).value()


            List<List<Class<?>>> classGroups = classes.collate(no)
            /*Class<?>[] classes = new Class[no]

            for (int i = 0; i < classes.length; i++) {
                classes[i] = clazz
            }*/

            //Manual Assignment of tests
            /* Class<?>[] classes = {
                    RegistrationTask.class,
                    RegistrationTask.class}*/
            // ParallelComputer(true,true) will run all classes and methods
            // in parallel.  (First arg for classes, second arg for methods)
            // I set true, true this means classes and methods runs in parallel.

            List<Result> resultList = []
            for (List<Class<?>> classGroup : classGroups) {
                Result result = JUnitCore.runClasses(new ParallelComputer(true, false), classGroup as Class<?>[])
                resultList.add(result)
            }

            //Result result = JUnitCore.runClasses(new ParallelComputer(true, false), classes)
        then:
            resultList.each { it ->
                println it.failures
                assert it.wasSuccessful()
            }

    }
}
