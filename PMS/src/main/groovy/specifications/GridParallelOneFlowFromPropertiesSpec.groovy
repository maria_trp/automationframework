package specifications

import org.junit.experimental.ParallelComputer
import org.junit.runner.JUnitCore
import org.junit.runner.Result
import spock.lang.Specification

import static commonPackage.tools.GetSystemParameters.getNumberOfThreads
import static commonPackage.tools.GetSystemParameters.getRunTest


//NOTES
/*To run in parallel specified flow you need to include the class name under SeleniumScreenshotListener class
on line 29, replace class with the Grid class you need to run
 */

class GridParallelOneFlowFromPropertiesSpec extends Specification {

    def "Grid parallel run of one class"() {

        when: "Multiple thread run in parallel mode for the same class"
            String noThreads = getNumberOfThreads()
            String testToRun = getRunTest()
            int no = Integer.parseInt(noThreads)

            Class<?> clazz = Class.forName(testToRun)

            Class<?>[] classes = new Class[no]

            for (int i = 0; i < classes.length; i++) {
                classes[i] = clazz
            }

            Result result = JUnitCore.runClasses(new ParallelComputer(true, true), classes)
        then: "All run results should be printed under proper index file"
            result.each { it ->
                println it.failures
                assert it.wasSuccessful()
            }

    }
}
