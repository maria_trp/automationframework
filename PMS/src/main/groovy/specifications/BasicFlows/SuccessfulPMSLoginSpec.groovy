package specifications.BasicFlows

import commonPackage.PMS.PMSBaseSpec
import commonPackage.PMS.pages.dashboard.DashboardPage

class SuccessfulPMSLoginSpec extends PMSBaseSpec {

    def "Successful PMS Login"() {

        given: "I navigate to Login page having an account having Platform and PMS access rights"
            String loginData = "PMSAccessUser"
            DashboardPage dashboardPage = loadDataAndNavigateToLoginPage(loginData)
            System.out.println("Navigation completed successfully")

        and: "I insert correct account credentials"
            dashboardPage.insertAccountDetails(loginData)
            System.out.println("Credentials successfully inserted")

        when: "I click on Sign In button"
            dashboardPage.clickSignInButton()
            System.out.println("Sign In button successfully clicked")

        then: "A welcome message should appear on Dashboard page"
            String actualMessage = dashboardPage.viewActualWelcomeMessage()
            String expectedMessage = dashboardPage.getExpectedWelcomeMessageFromJSON(loginData)
            assert actualMessage.contains(expectedMessage)
            System.out.println("Welcome message shown on screen")

        and: "I click on Logout button"
            dashboardPage.clickLogoutButton()
            System.out.println("User logged out successfully")

    }
}
