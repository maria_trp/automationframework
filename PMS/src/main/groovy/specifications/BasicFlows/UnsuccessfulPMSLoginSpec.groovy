package specifications.BasicFlows

import commonPackage.PMS.PMSBaseSpec
import commonPackage.PMS.pages.dashboard.DashboardPage

class UnsuccessfulPMSLoginSpec extends PMSBaseSpec {

    def "Unsuccessful PMS Login"() {

        given: "I navigate to Login page having an account with false password"
            String loginData = "FalseCredentialsUser"
            DashboardPage dashboardPage = loadDataAndNavigateToLoginPage(loginData)
            System.out.println("Navigation completed successfully")

        and: "I insert correct account credentials"
            dashboardPage.insertAccountDetails(loginData)
            System.out.println("Credentials successfully inserted")

        when: "I click on Sign In button"
            dashboardPage.clickSignInButton()
            System.out.println("Sign In button successfully clicked")

        then: "An error message should appear on screen"
            String actualMessage = dashboardPage.getIncorrectLoginDataMessage()
            assert actualMessage.contains("incorrectly")
            System.out.println("Error message shown on screen")
    }
}
