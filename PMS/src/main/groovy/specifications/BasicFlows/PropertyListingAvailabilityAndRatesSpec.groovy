package specifications.BasicFlows

import commonPackage.PMS.PMSBaseSpec
import commonPackage.PMS.jsonbin.PMSData
import commonPackage.PMS.pages.dashboard.DashboardPage
import commonPackage.PMS.pages.propertyListing.PriceCalculatorSection
import commonPackage.PMS.pages.propertyListing.PropertyListingPage
import commonPackage.tools.JsonDataLoader

class PropertyListingAvailabilityAndRatesSpec extends PMSBaseSpec {

    def "Successful Listing Insertion"() {

        given: "I login to PMS with valid credentials"
            String loginData = "PMSAccessUser"
            DashboardPage dashboardPage = loginToPMS(loginData)
            System.out.println("User logged in successfully")

        when: "I click on List your property button"
            PropertyListingPage propertyListingPage = dashboardPage.clickListYourPropertyButton()
            System.out.println("List your property button successfully clicked")

        and: "I fill in property name and type details section"
            String propertyDetails = "PropertyListingWithAvailabilityAndRates"
            propertyListingPage.fillInPropertyNameTypeDetails(propertyDetails)
            System.out.println("Property details successfully filled")

        and: "I fill in the accurate property details section"
            propertyListingPage.fillInAccuratePropertyDetails(propertyDetails)
            System.out.println("Accurate property details successfully filled")

        and: "I fill in the description property section"
            propertyListingPage.fillInPropertyDescriptions(propertyDetails)
            System.out.println("Description details successfully filled")

        and: "I fill in Sleeping arrangement details section"
            propertyListingPage.fillSleepingArrangementDetails()
            System.out.println("Sleeping arrangements details successfully filled")

        and: "I fill in Amenities details section"
            propertyListingPage.fillInAmenitiesDetails()
            System.out.println("Amenities details successfully filled")

        and: "I fill in Host Reception details section"
            propertyListingPage.fillInHostReceptionDetails()
            System.out.println("Host Reception details successfully filled")

        and: "I fill in House Rules details section"
            propertyListingPage.fillInHouseRulesDetails(propertyDetails)
            System.out.println("House Rules details successfully filled")

        and: "I upload photos under Photos section"
            propertyListingPage.UploadPhotosSection()
            System.out.println("House Rules details successfully filled")

        and: "I fill in Price per night section"
            propertyListingPage.fillPricePerNight(propertyDetails)
            System.out.println("Price per night successfully filled")

        and: "I fill in long term offer minimum stay section"
            propertyListingPage.fillMinimumStayWithDiscount(propertyDetails)
            System.out.println("Minimum stay with discount successfully filled")

        and: "I fill in Last minute with discount section"
            propertyListingPage.fillLastMinuteWithDiscount(propertyDetails)
            System.out.println("Last minute with discount successfully filled")

        and: "I click on Next button"
            propertyListingPage.clickPricingNextButton()
            System.out.println("Next button successfully clicked")

        and: "I click on Price Calculator button"
            propertyListingPage.clickPriceCalculatorButton()
            System.out.println("Price calculator button successfully clicked")

        and: "I insert dates and guests under price calculator section"
            PriceCalculatorSection priceCalculatorSection = new PriceCalculatorSection(driver, timings)
            priceCalculatorSection.insertPriceCalculatorDetails(propertyDetails)
            System.out.println("Insert price calculator details successfully completed")

        when: "I click on Calculate button"
            priceCalculatorSection.clickCalculateButton()
            System.out.println("Calculate button successfully clicked")

        then: "The proper Price per Night should be displayed"
            PMSData priceCalculatorDetailsData = JsonDataLoader.loadFromClassJson(propertyDetails, PMSData.class)
            priceCalculatorSection.verifyPricePerNight().contains(priceCalculatorDetailsData.getCalculatorPricePerNight())
            System.out.println("Proper price per night is displayed")

        and: "The proper Discount should be displayed"
            priceCalculatorSection.verifyDiscounts().contains(priceCalculatorDetailsData.getCalculatorDiscounts())
            System.out.println("Proper Discount is displayed")

        and: "The proper Total Payout should be displayed"
            priceCalculatorSection.verifyTotalPayout().contains(priceCalculatorDetailsData.getCalculatorsTotalPayout())
            System.out.println("Proper Total Payout is displayed")

        when: "I select availability dates"
            propertyListingPage.selectRatesAndAvailability()
            System.out.println("Availability successfully selected")

        and: "I select policy section"
            propertyListingPage.selectFlexiblePolicy()
            System.out.println("Flexible policy successfully selected")

        then: "A validation message should appear on screen to verify successful property listing"
            PMSData propertyDetailsData = JsonDataLoader.loadFromClassJson(propertyDetails, PMSData.class)
            propertyListingPage.getSuccessfulListingMessage().contains(propertyDetailsData.getSuccessfulListingMessage())
            System.out.println("Successful property listing message appeared on screen")

    }
}
