package specifications.BasicFlows

import commonPackage.PMS.PMSBaseSpec
import commonPackage.PMS.pages.dashboard.DashboardPage
import commonPackage.PMS.pages.homepage.HomepagePage

class SuccessfulPMSSignUpSpec extends PMSBaseSpec {

    def "Successful PMS Login"() {

        given: "I navigate to Login page"
            DashboardPage dashboardPage = navigateToPMSPageWithoutLoginData()
            System.out.println("Navigation to PMS completed successfully")

        when: "I click on Sign Up now button"
            dashboardPage.clickSignUpButton()
            System.out.println("Sign up button successfully pressed!")

        and: "I fill in all the fields in the form"
            String signUpData = "SignUpData"
            HomepagePage homepagePage = new HomepagePage(driver, timings)
            homepagePage.fillInSignUpForm(signUpData)
            System.out.println("Sign up form is successfully filled!")

        then: "I should be redirected to PMS"
            dashboardPage.verifySuccessfulSignUp().contains("Change profile")
            System.out.println("User is successfully signed up!")
    }
}
