package specifications.BasicFlows

import commonPackage.PMS.PMSBaseSpec
import commonPackage.PMS.jsonbin.PMSData
import commonPackage.PMS.pages.dashboard.DashboardPage
import commonPackage.PMS.pages.myListings.MyListingsPage
import commonPackage.tools.JsonDataLoader

class SearchListingSpec extends PMSBaseSpec {

    def "Successful Listing Insertion"() {

        given: "I login to PMS with valid credentials"
            String loginData = "PMSAccessUser"
            DashboardPage dashboardPage = loginToPMS(loginData)
            System.out.println("User logged in successfully")

        and: "I navigate to My Listing menu"
            MyListingsPage myListingsPage = dashboardPage.clickMyListingsMenuButton()
            System.out.println("My Listings menu button successfully clicked")

        when: "I search with an already listed property at search bar"
            String propertyDetailsToSearch = "SearchListingData"
            myListingsPage.searchPropertyListing(propertyDetailsToSearch)
            System.out.println("Property successfully searched through search bar")

        then: "Search results should contain the proper property item"
            PMSData propertyDetailsData = JsonDataLoader.loadFromClassJson(propertyDetailsToSearch, PMSData.class)
            myListingsPage.getSearchResult().contains(propertyDetailsData.getPropertyListingToSearch())

        and: "I click on Logout button"
            dashboardPage.clickLogoutButton()
            System.out.println("User logged out successfully")
    }
}
