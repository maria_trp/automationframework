package specifications.BasicFlows

import commonPackage.PMS.PMSBaseSpec
import commonPackage.PMS.jsonbin.PMSData
import commonPackage.PMS.pages.dashboard.DashboardPage
import commonPackage.PMS.pages.myListings.MyListingsPage
import commonPackage.tools.JsonDataLoader

class ChannelManagerActivationSpec extends PMSBaseSpec {

    def "Successful Listing Insertion"() {

        given: "I login to PMS with valid credentials"
            String loginData = "PMSAccessUser"
            DashboardPage dashboardPage = loginToPMS(loginData)
            System.out.println("User logged in successfully")

        and: "I navigate to My Listing menu"


        when: "I search with an already listed property at search bar"
            String propertyDetailsToSearch = "SearchListingData"

        then: "Search results should contain the proper property item"
            PMSData propertyDetailsData = JsonDataLoader.loadFromClassJson(propertyDetailsToSearch, PMSData.class)

        and: "I click on Logout button"

    }
}
