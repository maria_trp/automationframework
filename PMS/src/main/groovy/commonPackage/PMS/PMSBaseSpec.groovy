package commonPackage.PMS

import commonPackage.BaseSpec
import commonPackage.PMS.jsonbin.PMSData
import commonPackage.PMS.pages.dashboard.DashboardPage
import commonPackage.pages.LoginPage
import commonPackage.tools.JsonDataLoader
import spock.lang.Shared

import java.sql.Connection
import java.util.logging.Level
import java.util.logging.Logger

class PMSBaseSpec extends BaseSpec {

    public static PMSData currentPMSData

    @Shared
    DashboardPage dashboardPage

    def setup() {
    }

    DashboardPage loadDataAndNavigateToLoginPage(String data) throws Exception {
        JsonDataLoader.loadFromClassJson(data, PMSData.class)
        navigateToPMSLoginPage()

        return new DashboardPage(driver, timings)
    }

    DashboardPage loginToPMS(String data) throws Exception {
        currentPMSData = JsonDataLoader.loadFromClassJson(data, PMSData.class)
        dashboardPage = navigateToLoginPage().login(currentPMSData.getUsername(), currentPMSData.getPassword(), DashboardPage.class)

        return dashboardPage
    }

    void navigateToPMSPageWithoutLoginData() throws Exception {
        navigateToPMSLoginPage()
    }

    void userLogout() throws Exception {
        try {
            dashboardPage.logoutWeb()
        } catch (Exception ex) {
            Logger.getLogger(PMSBaseSpec.class.getName()).log(Level.SEVERE, null, ex)
            throw ex
        }
    }

    String runAnyQuery(String queryText) throws Exception {
        logger.info("Query to run is: " + queryText)
        Connection connection = DataBaseManager.openDB()
        String resultQuery = DataBaseManager.selectDB(connection, queryText)
        logger.info(": Result Query is:" + resultQuery)
        DataBaseManager.closeDB(connection)

        return resultQuery
    }

}

