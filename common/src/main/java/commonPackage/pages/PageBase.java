package commonPackage.pages;

import commonPackage.NavigationBase;
import commonPackage.tools.FileHelper;
import commonPackage.tools.SetLogger;
import commonPackage.tools.TimingsManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PageBase extends NavigationBase {

    protected static Logger logger = Logger.getLogger(PageBase.class);
    public final String MAIN_MENU = ".//div[@class='menu__control']";
    public final String LOG_OUT_BUTTON = ".//div[@class='menu__control']";


    protected WebDriver driver;
    protected TimingsManager timings;
    protected static FileHelper fileHelper = new FileHelper();

    public PageBase(WebDriver driver) throws Exception {
        super(driver);
        this.driver = driver;
        SetLogger.setLoggerLevel(logger);
    }

    public PageBase(WebDriver driver, TimingsManager timings) throws Exception {
        super(driver, timings);
        this.driver = driver;
        this.timings = timings;
        SetLogger.setLoggerLevel(logger);
    }

    public void logoutWeb() throws Exception {
        logOut();
    }

    //logout from the application
    public void logOut() throws Exception {
        waitUntilAttributeNotEmpty(LOG_OUT_BUTTON);
        clickButton(LOG_OUT_BUTTON);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void tearDown() {
        driver.close();
        driver.quit();
    }

    public static String testDateFormatter (){
        final LocalDateTime localtimestamp = LocalDateTime.now();
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.SSS");
        return localtimestamp.format(formatter);
    }
}
