package commonPackage.pages;

import commonPackage.tools.TimingsManager;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Constructor;

import static commonPackage.elementrepository.LoginPageObjects.*;

public class LoginPage extends PageBase {

    public LoginPage(WebDriver driver, TimingsManager timings) throws Exception {
        super(driver, timings);
        waitUntilElementIsVisible(USERNAME);
        waitUntilElementIsVisible(PASSWORD);
    }

    public <T> T login(String username, String password, Class<T> page) throws Exception {
        //clickButton(ACCEPT_COOKIES);
        type(USERNAME, username);
        type(PASSWORD, password);
        clickButton(SIGN_IN_BUTTON);
        Constructor<T> constructor = page.getConstructor(WebDriver.class, TimingsManager.class);
        return constructor.newInstance(driver, timings);

    }

}
