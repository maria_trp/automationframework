package commonPackage.exceptions;

public class ServiceErrorException extends Exception {

    public ServiceErrorException(String service, String description) {
        super("Service Error occurred... Check the screenshot...\nService: "+service+"\nDescription: "+description);
    }
}
