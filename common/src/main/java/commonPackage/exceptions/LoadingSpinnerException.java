package commonPackage.exceptions;

public class LoadingSpinnerException extends Exception {

    LoadingSpinnerException(String message) {
        super(message);
    }
}
