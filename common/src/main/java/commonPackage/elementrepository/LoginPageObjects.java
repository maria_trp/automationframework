package commonPackage.elementrepository;

public class LoginPageObjects {

    public static final String USERNAME = "//body/div[@id='main_content']/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/input[1]";
    public static final String PASSWORD = ".//body/div[@id='main_content']/div[1]/div[1]/div[1]/div[1]/form[1]/div[2]/input[1]";
    public static final String SIGN_IN_BUTTON = ".//body/div[@id='main_content']/div[1]/div[1]/div[1]/div[1]/form[1]/div[5]/button[1]";
    public static final String ACCEPT_COOKIES = ".//div[@class='cc-compliance']";

}
