package commonPackage;

import commonPackage.exceptions.ServiceErrorException;
import commonPackage.pages.PageBase;
import commonPackage.tools.*;
import commonPackage.tools.FileHelper;
import commonPackage.tools.SetLogger;
import commonPackage.tools.TimingsManager;
import org.apache.log4j.Logger;
import org.awaitility.Awaitility;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.lang.reflect.Constructor;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;


public class NavigationBase {

    public static final By SERVICE_ERROR = By.xpath(".//div[@id='error-dialog_layer']");

    protected static Logger logger = Logger.getLogger(PageBase.class);

    protected WebDriver driver;
    protected TimingsManager timings;
    protected static FileHelper fileHelper = new FileHelper();

    public NavigationBase(WebDriver driver) {
        this.driver = driver;
        SetLogger.setLoggerLevel(logger);
    }

    public NavigationBase(WebDriver driver, TimingsManager timings) {
        this.driver = driver;
        this.timings = timings;
        SetLogger.setLoggerLevel(logger);
    }


    public WebDriver getDriver() {
        return driver;
    }

    private void checkForServiceError() throws ServiceErrorException {
        if (!driver.findElements(SERVICE_ERROR).isEmpty()) {
            String service;
            String description;
            try {
                service = getMessage(".//div[@data-bind='text: serviceName']");
                description = getMessage(".//div[@data-bind='text: serviceErrorDescr']");
            } catch (Exception e) {
                service = "";
                description = "";
            }
            throw new ServiceErrorException(service, description);
        }
    }

    public <T> T returnPageObject(Class<T> clazz) throws Exception {
        Constructor constructor = clazz.getConstructor(WebDriver.class, TimingsManager.class);
        return clazz.cast(constructor.newInstance(driver, timings));
    }

    public void loadPage(String detailIdentifier) throws Exception {
        waitUntilPageisLoaded();
        waitUntilInvisibilityofElement(".//*[contains(@class,'spinner')]");
        checkForServiceError();
        waitUntilElementLocated(detailIdentifier);
    }

    public void loadButton(String detailIdentifier) throws Exception {
        waitUntilPageisLoaded();
        waitUntilInvisibilityofElement(".//div[contains(@class,'spinner')]");
        checkForServiceError();
        waitUntilElementLocated(detailIdentifier);
    }

    public void clickButton(String detailIdentifier) throws Exception {
        loadPage(detailIdentifier);
        waitUntilPageisCompleted();
        waitUntilElementIsVisible(detailIdentifier);
        waitUntilElementisClickable(detailIdentifier);
        driver.findElement(By.xpath(detailIdentifier)).click();
        checkForServiceError();
    }

    public void hardClickButton(String detailIdentifier) throws Exception {
        waitUntilPageisCompleted();
        waitUntilElementisClickable(detailIdentifier);
        driver.findElement(By.xpath(detailIdentifier)).click();
        checkForServiceError();
    }

    public void clickLoadedButton(String detailIdentifier) throws Exception {
        loadButton(detailIdentifier);
        waitUntilElementisClickable(detailIdentifier);
        driver.findElement(By.xpath(detailIdentifier)).click();
        checkForServiceError();
        waitUntilPageisCompleted();
    }

    public void clickInvisibleElement(String Identifier) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement elem = driver.findElement(By.xpath(Identifier));
        String javascript = "arguments[0].style.display='inline-block'; arguments[0].style.visibility='visible';";
        js.executeScript(javascript, elem);
        elem.click();
    }

    public void clickbyJavascript(String detailIdentifier) {
        WebElement element = driver.findElement(By.xpath(detailIdentifier));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }

    public void clickCheckBox(String detailIdentifier) throws Exception {
        waitUntilElementLocated(detailIdentifier);
        WebElement button = driver.findElement(By.xpath(detailIdentifier));
        button.click();
    }

    public void clickCheckBox(String detailIdentifier, String text) throws Exception {
        if (checkIfValueIsSet(text)) {
            clickCheckBox(detailIdentifier);
        }
    }

    public void clickLink(String detailIdentifier) throws Exception {
        waitUntilElementLocated(detailIdentifier);
        WebElement link = driver.findElement(By.xpath(detailIdentifier));
        link.click();
    }

    public void clickRadioBtn(String detailIdentifier) throws Exception {
        waitUntilElementLocated(detailIdentifier);
        WebElement radioBtn = driver.findElement(By.xpath(detailIdentifier));
        radioBtn.click();
    }

    public void clickRadioBtn(String detailIdentifier, Boolean toBeClicked) throws Exception {
        if (toBeClicked) {
            clickRadioBtn(detailIdentifier);
        }
    }

    public void hardType(String detailIdentifier, String text) throws Exception {
        waitUntilElementLocated(detailIdentifier);
        driver.findElement(By.xpath(detailIdentifier))
                .sendKeys(text);
    }

    public void type(String detailIdentifier, String text) throws Exception {
        if (checkIfValueIsSet(text)) {
            waitUntilElementLocated(detailIdentifier);
            waitUntilElementIsVisible(detailIdentifier);
            waitUntilElementisClickable(detailIdentifier);
            WebElement editbox = driver.findElement(By.xpath(detailIdentifier));
            switch (text) {
                case "randomText":
                    text = Generator.randomText();
                    break;
                case "sysdate":
                    text = Utils.formatDate("dd/MM/yyyy", Generator.getSysDate());
                    break;
            }
            //send the new text by sending CTRL+a to select all previous text (if any) and resending the new text
            //editbox.sendKeys(Keys.chord(Keys.CONTROL, "a"), text);
            editbox.sendKeys(Keys.chord(Keys.CONTROL, "a"));
            editbox.sendKeys(text);
            checkForServiceError();
        }
    }

    public void browseFile(String filePath) throws Exception {
        StringSelection ss = new StringSelection(filePath);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, ss);
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }

    public void clear(String detailIdentifier) throws Exception {
        waitUntilElementLocated(detailIdentifier);
        WebElement editbox = driver.findElement(By.xpath(detailIdentifier));
        editbox.clear();
    }

    public boolean checkIfValueIsSet(String value) {
        return value != null && !value.equals("");
    }

    public List<WebElement> getDropDownOptions(String selectButton, String resultsIdentifier) throws Exception {
        clickButton(selectButton);
        waitUntilElementLocated(resultsIdentifier);
        waitUntilElementIsVisible(resultsIdentifier);
        WebElement dropDown = driver.findElement(By.xpath(resultsIdentifier));
        return dropDown.findElements(By.xpath(".//li/div"));
    }

    public void selectDropDownListByIndex(String selectButton, String resultsIdentifier, Integer index) throws Exception {
        java.util.List<WebElement> allOptions = getDropDownOptions(selectButton, resultsIdentifier);
        allOptions.get(index).click();
        waitUntilPageisLoaded();
    }

//    public void waitUtilValueIsSet(String selectButton,String text){
//        waitUntilElementIsVisible(selectButton);
//
//        for(int i=0; i<=10; i++){
//
//
//        }
//    }

    public void selectDropDownList(String selectButton, String resultsIdentifier, String text) throws Exception {
        if (checkIfValueIsSet(text)) {
            java.util.List<WebElement> allOptions = getDropDownOptions(selectButton, resultsIdentifier);
            for (WebElement we : allOptions) {
                if (we.getText().contains(text)) {
                    we.click();
                    waitUntilPageisLoaded();
                    break;
                }
            }
        }
    }

    public void selectDropDownListWithHover(String selectButton, String resultsIdentifier, String text) throws Exception {
        if (checkIfValueIsSet(text)) {
            java.util.List<WebElement> allOptions = getDropDownOptions(selectButton, resultsIdentifier);
            hoverToElement(resultsIdentifier);
            for (WebElement we : allOptions) {
                if (we.getText().contains(text)) {
                    we.click();
                    waitUntilPageisLoaded();
                    break;
                }
            }
        }
    }

    public void selectExactMatchDropDownList(String selectButton, String resultsIdentifier, String text) throws Exception {
        if (checkIfValueIsSet(text)) {
            java.util.List<WebElement> allOptions = getDropDownOptions(selectButton, resultsIdentifier);
            for (WebElement we : allOptions) {
                if (we.getText().equalsIgnoreCase(text)) {
                    we.click();
                    waitUntilPageisLoaded();
                    break;
                }
            }
        }
    }

    public void selectDropDownListByOption(String detailIdentifier, String text) throws Exception {
        if (checkIfValueIsSet(text)) {
            WebElement dropDown = driver.findElement(By.xpath(detailIdentifier));
            java.util.List<WebElement> allOptions = dropDown.findElements(By.xpath(".//oj-option"));
            for (WebElement we : allOptions) {
                if (we.getText().contains(text)) {
                    we.click();
                    waitUntilPageisLoaded();
                    break;
                }
            }
        }
    }

    public void selectFirstMatchFromClassicDropdown(String identifier, String text) throws Exception {
        waitUntilElementLocated(identifier);
        waitUntilElementIsVisible(identifier);
        Select select = new Select(driver.findElement(By.xpath(identifier)));
        Awaitility.await().atMost(10, TimeUnit.SECONDS).until(() -> (select.getOptions() != null && select.getOptions().size() > 0));
        select.getOptions().stream().filter(it -> it.getText().contains(text)).findFirst().get().click();
    }

    public void selectFromClassicDropdown(String identifier, String text) throws Exception {
        waitUntilElementLocated(identifier);
        waitUntilElementIsVisible(identifier);
        Select select = new Select(driver.findElement(By.xpath(identifier)));
        Awaitility.await().atMost(10, TimeUnit.SECONDS).until(() -> select.getOptions().size() > 0);
        select.selectByVisibleText(text);
    }

    public void searchAndSelectList(String detailIdentifier, String detailIdentifierResults, String text) throws Exception {
        if (checkIfValueIsSet(text)) {
            type(detailIdentifier, text);
            waitUntilAttributeNotEmpty(detailIdentifierResults);
            WebElement dropDown = driver.findElement(By.xpath(detailIdentifierResults));
            java.util.List<WebElement> allOptions = dropDown.findElements(By.xpath(".//li/div"));
            for (WebElement we : allOptions) {
                if (we.getText().contains(text)) {
                    Thread.sleep(1000);
                    Actions builder = new Actions(getDriver());
                    builder.moveToElement(we).build().perform();
                    we.click();
                    waitUntilPageisLoaded();
                    break;
                }
            }
        }
    }

    public String getMessage(String detailIdentifier) throws Exception {
        waitUntilElementLocated(detailIdentifier);
        waitUntilElementIsVisible(detailIdentifier);
        return driver.findElement(By.xpath(detailIdentifier)).getText();
    }

    public String getTitle(String identifier) throws Exception {
        waitUntilElementIsVisible(identifier);
        waitUntilElementLocated(identifier);
        return driver.findElement(By.xpath(identifier)).getAttribute("title");
    }

    public String getElementValue(String identifier) throws Exception {
        waitUntilElementLocated(identifier);
        waitUntilElementIsVisible(identifier);
        return driver.findElement(By.xpath(identifier)).getAttribute("value");
    }

    public boolean elementContainsText(String detailIdentifier, String text) throws Exception {
        return getMessage(detailIdentifier).contains(text);
    }

    public void refreshPage() {
        driver.navigate().refresh();
    }

    public void refreshUntilElementIsVisible(String detailIdentifier) throws Exception {
        int i = 0;
        while (elementIsCurrentlyVisible(By.xpath(detailIdentifier)) == false && i < 30) {
            refreshPage();
            Thread.sleep(15000);
            i++;
        }
        return;
    }

    public boolean isAlertPresents() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void scrollDown() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,320)", "");

    }

    public void scrollUp() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(250,0)", "");

    }

    public void switchToParentWindow() {
        String parentHandle = driver.getWindowHandle();
        driver.switchTo().window(parentHandle);
    }

    public void switchToChildWindow() {
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
    }

    public void hoverToElement(String detailIdentifier) throws Exception {
        waitUntilElementLocated(detailIdentifier);
        waitUntilElementIsVisible(detailIdentifier);
        WebElement web_Element_To_Be_Hovered = driver.findElement(By.xpath(detailIdentifier));
        Actions builder = new Actions(getDriver());
        builder.moveToElement(web_Element_To_Be_Hovered).build().perform();
    }

    public void scrollToElement(String detailIdentifier) throws Exception {
        waitUntilElementLocated(detailIdentifier);
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        WebElement web_Element = driver.findElement(By.xpath(detailIdentifier));
        jse.executeScript("arguments[0].scrollIntoView();", web_Element);
    }

    public void hoverAndClickElement(String hoverItem, String clickItem) {
        WebElement web_Element_To_Be_Hovered = driver.findElement(By.xpath(hoverItem));
        Actions builder = new Actions(getDriver());
        builder.moveToElement(web_Element_To_Be_Hovered).perform();
        driver.findElement(By.xpath(clickItem)).click();
    }

    // ############################## WAIT METHODS##############################
    public Duration getWaitForTimeout() throws Exception {
        Duration waitForTimeout = null;
        int configuredWaitForTimeoutInMilliseconds;

        String WebDriverWait = GetSystemParameters.getWebDriverTimeOut();

        if (waitForTimeout == null) {
            configuredWaitForTimeoutInMilliseconds = Integer.parseInt(WebDriverWait);
            waitForTimeout = Duration.ofMillis(configuredWaitForTimeoutInMilliseconds);
        }
        return waitForTimeout;
    }

    public void waitUntilElementLocated(String detailIdentifier) throws Exception {
        WebDriverWait wait = (new WebDriverWait(driver, getWaitForTimeout().getSeconds()));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(detailIdentifier)));
    }

    public void waitUntilAllElementsLocated(String detailIdentifier) throws Exception {
        WebDriverWait wait = (new WebDriverWait(driver, getWaitForTimeout().getSeconds()));
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(detailIdentifier)));
    }

    public void waitUntilText(String detailIdentifier, String text) throws Exception {
        WebDriverWait wait = (new WebDriverWait(driver, getWaitForTimeout().getSeconds()));
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(detailIdentifier), text));
    }

    // Visibility means that the element is not just displayed but also should
    // also has a height and width that is greater than 0.
    public void waitUntilAttributeNotEmpty(String detailIdentifier) {
        WebDriverWait wait;
        try {
            wait = (new WebDriverWait(driver, getWaitForTimeout().getSeconds()));
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(detailIdentifier)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isLocatorVisible(String detailIdentifier) {
        WebDriverWait wait;
        try {
            wait = (new WebDriverWait(driver, getWaitForTimeout().getSeconds()));
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(detailIdentifier)));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void waitUntilInvisibilityofElement(String detailIdentifier) throws Exception {
        WebDriverWait wait = (new WebDriverWait(driver, getWaitForTimeout().getSeconds()));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(detailIdentifier)));
    }

    public void waitUntilInvisibilityofText(String detailIdentifier, String text) throws Exception {
        WebDriverWait wait = (new WebDriverWait(driver, getWaitForTimeout().getSeconds()));
        wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(detailIdentifier), text));
    }

    public void waitUntilVisibilityofText(String detailIdentifier, String text) throws Exception {
        WebDriverWait wait = (new WebDriverWait(driver, getWaitForTimeout().getSeconds()));
        wait.until(ExpectedConditions.textToBe(By.xpath(detailIdentifier), text));
    }

    // implicit wait.
    public void waitUntilPageisLoaded() throws Exception {
        driver.manage().timeouts().pageLoadTimeout(getWaitForTimeout().getSeconds(), TimeUnit.SECONDS);
    }

    private void waitPageReadyState() throws Exception {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        WebDriverWait wait = (new WebDriverWait(driver, getWaitForTimeout().getSeconds()));
        wait.until(driver -> jse.executeScript("return document.readyState").equals("complete"));
    }

    public void waitUntilPageCompletedAndLoaded() throws Exception {
        waitUntilPageisCompleted();
        waitUntilPageisLoaded();
    }

    public void waitUntilPageisCompleted() throws Exception {
        waitUntilPageisLoaded();
        checkForServiceError();
        waitAllSpinners();
        waitPageReadyState();
        checkForServiceError();
    }

    public void waitAllSpinners() throws Exception {
        waitUntilInvisibilityofElement(".//*[contains(@class,'spinner')]");
        waitUntilInvisibilityofElement(".//div[@class='spinnerContainer']");
        waitUntilInvisibilityofElement(".//div[@class='loadingIndicator-disable']");
        waitUntilInvisibilityofElement(".//div[contains(@class,'spinnerItem')]");
        waitUntilInvisibilityofElement(".//div[contains(@data-bind,'loadingSpinner.getstateObj')]");
    }

    public void waitUntilElementisClickable(String detailIdentifier) throws Exception {
        WebDriverWait wait = (new WebDriverWait(driver, getWaitForTimeout().getSeconds()));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(detailIdentifier)));
    }

    public boolean isElementClickable(String detailIdentifier) {
        return driver.findElement(By.xpath(detailIdentifier)).isEnabled();
    }

    public boolean elementIsCurrentlyVisible(final By byElementCriteria) {
        try {
            java.util.List<WebElement> matchingElements = driver.findElements(byElementCriteria);
            return (!matchingElements.isEmpty() && matchingElements.get(0).isDisplayed());
        } catch (NoSuchElementException | StaleElementReferenceException | TimeoutException noSuchElement) {
            return false;
        }
    }

    public void waitUntilElementIsVisible(String detailIdentifier) throws Exception {
        WebDriverWait wait = (new WebDriverWait(driver, getWaitForTimeout().getSeconds()));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(detailIdentifier)));
    }

    public boolean isLocatorDisplayed(String detailIdentifier) {
        try {
            return driver.findElement(By.xpath(detailIdentifier)).isDisplayed();
        } catch (Exception e) {
            logger.info("Locator " + detailIdentifier + " is not in DOM");
            logger.info(e);
        }
        return false;
    }

    public boolean hasLocatorAppearedAfterAWhile(String identifier, Integer seconds) {
        try {
            WebDriverWait wait = (new WebDriverWait(driver, seconds));
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(identifier)));
            return true;
        } catch (Exception e) {
            logger.info(e);
            return false;
        }
    }

    public java.util.List<WebElement> waitForElements(String identifier) throws Exception {
        WebDriverWait wait = (new WebDriverWait(driver, getWaitForTimeout().getSeconds()));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(identifier)));
        return driver.findElements(By.xpath(identifier));
    }

    // ############################## BROWSER METHODS##############################
    public void increaseBrowserBuffer() {
        String bufferbrowser = "window.performance.setResourceTimingBufferSize(20000);";
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript(bufferbrowser);
    }

    public void waitUntilAnAttributeHasValue(String identifier, String attribute, String value) {
        ExpectedCondition e = new ExpectedCondition() {
            public Boolean apply(Object d) {
                try {
                    return driver.findElement(By.xpath(identifier)).getAttribute(attribute).contains(value);
                } catch (WebDriverException e) {
                    return false;
                }
            }
        };
        WebDriverWait w = new WebDriverWait(driver, 50, 100L);
        w.until(e);
    }

    public void mouseOverJScript(String identifier) {
//The below JavaScript code creates, initializes and dispatches mouse event to an object on fly.
        String strJavaScript = "var element = arguments[0];"
                + "var mouseEventObj = document.createEvent('MouseEvents');"
                + "mouseEventObj.initEvent( 'mouseover', true, true );"
                + "element.dispatchEvent(mouseEventObj);";

        WebElement element = driver.findElement(By.xpath(identifier));

//Then JavascriptExecutor class is used to execute the script to trigger the dispatched event.
        ((JavascriptExecutor) driver).executeScript(strJavaScript, element);
    }

    public String waitUntilElementHasSpecificValue(String identifier, String value) throws Exception {
        Awaitility.await().atMost(20, TimeUnit.SECONDS).until(() -> getMessage(identifier).equals(value));
        return getMessage(identifier);
    }

    public String waitUntilElementContainsValue(String identifier, String value) throws Exception {
        Awaitility.await().atMost(20, TimeUnit.SECONDS).until(() -> getMessage(identifier).contains(value));
        return getMessage(identifier);
    }

    public int getElementsCount(String xpathElement) {
        java.util.List<WebElement> optionCount = driver.findElements(By.xpath(xpathElement));
        return optionCount.size();
    }

    public boolean stringContainsNonOfCriteria(String input, List<String> criteria) {
        return criteria.stream().noneMatch(input::contains);
    }

    public boolean stringContainsAllOfCriteria(String input, List<String> criteria) {
        return criteria.stream().allMatch(input::contains);
    }

    public void uploadFile(String fileInputIdentifier, String filePath) throws Exception {
        String path = fileHelper.getFilePathFromClassPath(filePath);
        WebElement fileInput = driver.findElement(By.xpath(fileInputIdentifier));
        logger.info("File path is: " + path);
        fileInput.sendKeys(path);
    }

    public void clickAndUploadFile(String fileInputIdentifier, String filePath) throws Exception {
        String path = fileHelper.getFilePathFromClassPath(filePath);
        WebElement fileInput = driver.findElement(By.xpath(fileInputIdentifier));
        fileInput.click();
        logger.info("File path is: " + path);
        fileInput.sendKeys(path);
    }

    public void uploadCreatedFile(String fileInputIdentifier, File file) throws Exception {
        String path = fileHelper.getFilePathFromLocalEnvironment(file);
        WebElement fileInput = driver.findElement(By.xpath(fileInputIdentifier));
        logger.info("File path is: " + path);
        fileInput.sendKeys(path);
    }

    public <T> Object createClassInstance(String className) throws Exception {
        return Class.forName(className)
                .getConstructor(WebDriver.class, TimingsManager.class)
                .newInstance(driver, timings);
    }

    public boolean isFieldEditable(String identifier) throws Exception {
        try {type(identifier,"random text");}
        catch (Exception e) {
            return false;
        }
        return true;
    }

    public java.util.List<WebElement> getWebElements(String Identifier) throws Exception{
        return driver.findElements(By.xpath(Identifier));
    }

    public java.util.List<String> getMulipleMessages(List<WebElement> List) throws Exception{
        List<String> list = new ArrayList<>();
        for(WebElement element: List){
            System.out.println(element.getText());
            list.add(element.getText());

        }
        return list;
    }

    public boolean listContainsText(List<String> List, String value) throws Exception{
        return List.contains(value);
    }

    public List<String> getAttributeFromElements(String detailIdentifier, String attribute) throws Exception {
        List<String> listOfAttributes = new ArrayList<>();
        List<WebElement> elements = driver.findElements(By.xpath(detailIdentifier));
        for(WebElement element: elements){
            listOfAttributes.add(element.getAttribute(attribute));
        }
        return listOfAttributes;

    }

}