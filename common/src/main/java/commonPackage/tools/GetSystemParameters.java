package commonPackage.tools;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

public class GetSystemParameters {

    protected static Logger logger = Logger.getLogger(GetSystemParameters.class);

    public static int getNumberOfTries() throws IOException {
        return Integer.parseInt(getSystemFileProperty("NUMBER_OF_TRIES"));
    }

    public static String getSystemPropertyFile() {
        return "/system.properties";
    }

    public static String getConfPropertyFile() {
        String confFilePathPattern = "/{0}{1}_{2}_conf.properties";
        final String confFile = MessageFormat.format(confFilePathPattern, getConfFilePath(), getCustomerName(), getEnvironmentName());
        return confFile;
    }

    public static String getPropFromPropertiesFile(String fileName, String propertyName) {
        Properties conf = new Properties();
        InputStream fs= GetSystemParameters.class.getResourceAsStream(fileName);

        try {
            conf.load(fs);
            fs.close();
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
        return conf.getProperty(propertyName);
    }

    public static String getBrowserDriver() {
        return getSystemFileProperty("driver.name");
    }

    public static String getBrowser() {
        return getSystemFileProperty("browser.name");
    }

    public static String getOs() {
        return getSystemFileProperty("os.name");
    }

    public static String getVersion() {
        return getSystemFileProperty("version.name");
    }

    public static String getUrl() {
        return getSystemFileProperty("url");
    }

    public static String getUserName() {
        return getSystemFileProperty("username");
    }

    public static String getAccessKey() {
        return getSystemFileProperty("access_key");
    }

    public static String getPerformanceMode() {
        return getSystemFileProperty("PERFORMANCE_MODE");
    }

    public static String getAssertionMode() {
        return getSystemFileProperty("ASSERTION_MODE");
    }

    public static String getInputDataJson() {
        String pathPattern = "{0}{1}_{2}_InputData.json";
        String inputDataEnvFile = defineInputDataFile();
        String jsonFilePath = MessageFormat.format(pathPattern, getInputJsonPath(), getCustomerName(), inputDataEnvFile);
        return jsonFilePath;
    }

    public static String getCustomerName() {
        return getSystemFileProperty("customer.name");
    }

    public static String getEnvironmentName() {
        return getSystemFileProperty("environment.name");
    }

    public static String defineInputDataFile() {
        if (getCustomerName().equals("CORE")) {
            return "INTERNAL";
        } else {
            return "EXTERNAL";
        }
    }

    public static String getInputJsonPath() {
        return getSystemFileProperty("inputDataJsonPath");
    }

    public static String getConfFilePath() {
        return getSystemFileProperty("confFilePath");
    }

    public static String getDbUrl() {
        return getConfFileProperty("DB_URL");
    }

    public static String getDbUser() {
        return getConfFileProperty("DB_USER");
    }

    public static String getDbPassword() {
        return getConfFileProperty("DB_PASSWORD");
    }

    public static String getInitialContectFactory() {
        return getSystemFileProperty("INITIAL_CONTEXT_FACTORY");
    }

    public static String getProviderUrl() {
        return getConfFileProperty("PROVIDER_URL");
    }

    public static String getSecurityPrincipal() {
        return getConfFileProperty("SECURITY_PRINCIPAL");
    }

    public static String getSecurityCredentials() {
        return getConfFileProperty("SECURITY_CREDENTIALS");
    }

    public static String getWebDriverTimeOut() {
        return getSystemFileProperty("WEBDRIVER_WAIT_FOR_TIMEOUT");
    }

    public static String getNumberOfThreads() {
        return getConfFileProperty("NUMBER_OF_THREADS");
    }

    public static String getRunTest() {
        return getConfFileProperty("RUN_TEST");
    }

    public static String getLoggerLevel() {
        return getSystemFileProperty("LOGGER_LEVEL");
    }

    public static String getConfFileProperty(String property) {
        return getPropFromPropertiesFile(getConfPropertyFile(), property);
    }

    public static String getSystemFileProperty(String property) {
        return System.getProperty(property, getPropFromPropertiesFile(getSystemPropertyFile(), property));
    }

}
