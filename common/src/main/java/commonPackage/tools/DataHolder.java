package commonPackage.tools;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DataHolder {

    public static final String RANDOM_UUID = "randomUUID";
    public static final String RANDOM_TEXT = "randomText";
    public static final String USER = "user";

    private Map<String, String> values = new HashMap<String, String>();

    public void setValue(String key, String value) {
        values.put(key, value);

    }

    public String getValue(String key) {
        return values.get(key);
    }

    public Set<Entry<String, String>> getEntries() {
        return values.entrySet();
    }

}
