package commonPackage.tools;

import org.apache.log4j.Logger;
import org.w3c.dom.NodeList;

import javax.xml.soap.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import static commonPackage.tools.GetSystemParameters.getConfFileProperty;

public class SoapClient {

    protected static Logger logger = Logger.getLogger(SoapClient.class);

    public static SOAPMessage sendSoapRequest(String urlSoap, String requestFile, Map<String, String> params)
            throws Exception {

        String SOAPUrl = getConfFileProperty(urlSoap);
        String xmlFile2Send = getConfFileProperty(requestFile);
        String xml = String.join("\n", Files.readAllLines(Paths.get(xmlFile2Send)));

        // replacing values in Request XML
        for (Map.Entry<String, String> param : params.entrySet()) {
            xml = xml.replaceAll(param.getKey(), param.getValue());
        }

        ByteArrayInputStream xmlStream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
        SOAPMessage soapRequest = createSOAPRequest(xmlStream);
        SOAPMessage soapResponse = getSOAPResponse(soapRequest, SOAPUrl);
        logger.info(soapMessageToString(soapResponse));
        return soapResponse;

    }

    public static SOAPMessage sendSoapRequestWithoutParams(String urlSoap, String requestFile)
            throws Exception {

        String SOAPUrl = getConfFileProperty(urlSoap);
        String xmlFile2Send = getConfFileProperty(requestFile);
        String xml = String.join("\n", Files.readAllLines(Paths.get(xmlFile2Send)));
        ByteArrayInputStream xmlStream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
        SOAPMessage soapRequest = createSOAPRequest(xmlStream);
        SOAPMessage soapResponse = getSOAPResponse(soapRequest, SOAPUrl);
        logger.info("Soap response is " + soapMessageToString(soapResponse));
        return soapResponse;
    }

    private static SOAPMessage createSOAPRequest(InputStream content) throws Exception {

        // Create a SOAP message from the XML content
        MessageFactory factory = MessageFactory.newInstance();
        SOAPMessage message = factory.createMessage(new MimeHeaders(), content);
        return message;
    }

    private static SOAPMessage getSOAPResponse(SOAPMessage soapRequest, String strEndpoint)
            throws Exception, SOAPException {

        // Send the SOAP request to the given endpoint and return the corresponding
        // response
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();
        SOAPMessage soapResponse = soapConnection.call(soapRequest, strEndpoint);
        return soapResponse;
    }

    public static String getValueFromSOAPResponse(SOAPMessage soapResponse, String tagName)
            throws Exception, SOAPException {

        SOAPBody body = soapResponse.getSOAPBody();
        NodeList list = body.getElementsByTagName(tagName);
        return list.item(0).getFirstChild().getNodeValue();

    }

    public static String soapMessageToString(SOAPMessage message) {
        String result = null;

        if (message != null) {
            ByteArrayOutputStream baos = null;
            try {
                baos = new ByteArrayOutputStream();
                message.writeTo(baos);
                result = baos.toString();
            } catch (Exception e) {
            } finally {
                if (baos != null) {
                    try {
                        baos.close();
                    } catch (IOException ioe) {
                    }
                }
            }
        }
        return result;
    }

}
