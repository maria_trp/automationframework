package commonPackage.tools;

import java.io.File;
import java.net.URLDecoder;

public class FileHelper {

    public String getFilePathFromClassPath(String fullFilePath) throws Exception {
        String relativeResourcePath = fullFilePath.split("resources")[1].substring(1);
        ClassLoader classLoader = getClass().getClassLoader();
        String path = classLoader.getResource(relativeResourcePath).getPath()
                .replace("/C:", "C:")
                .replace("/", "\\");
        return URLDecoder.decode(path, "UTF-8");
    }

    public String getFilePathFromLocalEnvironment(File file) throws Exception {
        String path = file.getPath()
                .replace("/C:", "C:")
                .replace("/", "\\");
        return URLDecoder.decode(path, "UTF-8");
    }
}
