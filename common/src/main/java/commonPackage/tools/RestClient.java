package commonPackage.tools;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.file.Files;
import java.nio.file.Paths;

import static commonPackage.tools.GetSystemParameters.getConfFileProperty;

public class RestClient {

    protected static Logger logger = Logger.getLogger(RestClient.class);

    public static JSONObject putJSONValue(JSONObject jsonobj, String key, String value) {
        return jsonobj.put(key, value);
    }

    public static JSONObject putJSONValue(JSONArray jsonArray, String key, String value, int index) {
        return jsonArray.getJSONObject(index).put(key, value);
    }

    public static JSONObject getJSONObject(JSONObject obj, String getObject) {
        JSONObject jsonObj = (JSONObject) obj.get(getObject);
        return jsonObj;

    }

    public static JSONObject getJSONObject(JSONArray jsonArray, String getObject, int index) {
        JSONObject jsonObj = (JSONObject) jsonArray.getJSONObject(index).get(getObject);
        return jsonObj;

    }

    public static JSONArray getJSONArray(JSONObject obj, String getArray) {
        JSONArray jsonArray = obj.getJSONArray(getArray);
        return jsonArray;
    }

    public static String getJSONFile(String jsonFile) throws Exception {
        String json = String.join("\n", Files.readAllLines(Paths.get(getConfFileProperty(jsonFile))));
        return json;

    }

}
