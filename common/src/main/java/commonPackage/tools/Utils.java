package commonPackage.tools;

import org.apache.log4j.Logger;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Utils {

    protected static Logger logger = Logger.getLogger(Utils.class);

    public static String formatDate(String pattern, LocalDate dateToFormat) throws Exception {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);
        String dateFormatted = dtf.format(dateToFormat);
        logger.info(dateFormatted);
        return dateFormatted;
    }
}
