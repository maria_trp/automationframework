package commonPackage.tools;

import commonPackage.pages.PageBase;
import org.openqa.selenium.WebDriver;

import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

public class Generator extends PageBase {

    public Generator(WebDriver driver) throws Exception {
        super(driver);

    }

    public static String generateUUID() {

        UUID idOne = UUID.randomUUID();
        logger.info("UUID: " + idOne);
        String sid = idOne.toString();
        return sid.replace("-", "");
    }

    public static String randomNumbers() throws Exception {
        final String lexicon = "0123456789";
        final java.util.Random rand = new java.util.Random();
        final Set<String> theset = new HashSet<String>();

        StringBuilder builder = new StringBuilder();
        while (builder.toString().length() == 0) {
            int length = 8;
            for (int i = 0; i < length; i++) {
                builder.append(lexicon.charAt(rand.nextInt(lexicon.length())));
            }
            if (theset.contains(builder.toString())) {
                builder = new StringBuilder();
            }
        }
        logger.info("Random Text is: " + builder);
        return builder.toString();

    }

    public static Long generateRandomNumber(int len) {
        String numbersWithoutZero = "123456789";
        String numbers = "0123456789";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(len);
        sb = sb.append(numbersWithoutZero.charAt(rnd.nextInt(numbersWithoutZero.length())));
        for (int i = 1; i < len; i++) {
            sb.append(numbers.charAt(rnd.nextInt(numbers.length())));
        }
        return Math.abs(Long.parseLong(sb.toString()));
    }

    public static String randomText() {

        final String lexicon = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final java.util.Random rand = new java.util.Random();
        final Set<String> identifiers = new HashSet<String>();

        StringBuilder builder = new StringBuilder();
        while (builder.toString().length() == 0) {
            int length = 9;
            for (int i = 0; i < length; i++) {
                builder.append(lexicon.charAt(rand.nextInt(lexicon.length())));
            }
            if (identifiers.contains(builder.toString())) {
                builder = new StringBuilder();
            }
        }
        //logger.info("Random Text is: " + builder);
        return builder.toString();
    }

    public static LocalDate getSysDate() {
        LocalDate localDate = LocalDate.now();
        return localDate;
    }
}
