package commonPackage.tools;

import java.text.MessageFormat;
import java.util.Map;

import static commonPackage.tools.GetSystemParameters.*;

public class JsonDataLoader {

    public static <T> T loadFromClassJson(String key, Class<T> clazz) {
        String pathPattern = "{0}{1}_{2}_{3}_InputData.json";
        String inputDataEnvFile = defineInputDataFile();
        String jsonFilePath = MessageFormat.format(pathPattern, getInputJsonPath(), getCustomerName(), inputDataEnvFile, clazz.getSimpleName());

        Map<String, T> data = JsonParser.readFile(jsonFilePath, clazz);

        if (data != null) {
            return data.get(key);
        }
        return null;
    }

    public static <T> T loadFromGeneralJson(String key, Class<T> clazz) {
        String jsonFilePath = getInputDataJson();

        Map<String, T> data = JsonParser.readFile(jsonFilePath, clazz);

        if (data != null) {
            return data.get(key);
        }
        return null;
    }
}
