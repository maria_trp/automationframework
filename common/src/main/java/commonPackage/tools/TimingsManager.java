package commonPackage.tools;

import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.UUID;

public class TimingsManager {

    protected static Logger logger = Logger.getLogger(TimingsManager.class);

    public Long starttime = 0L;

    private String getBrowserVersion(WebDriver driver) {
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browserName = cap.getBrowserName().toLowerCase();
        logger.info(browserName);
        return browserName;
    }

    public void clearBrowserMemory(WebDriver driver) {
        String clearbrowser = "performance.clearResourceTimings()";
        String clearMarks = "window.performance.clearMarks()";
        String clearMeasures = "performance.clearMeasures()";
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript(clearbrowser);
        jse.executeScript(clearMarks);
        jse.executeScript(clearMeasures);
    }

    private String performanceMode() throws Exception {
        String mode = GetSystemParameters.getPerformanceMode();
        return mode;
    }

    private String assertMode() throws Exception {
        String mode = GetSystemParameters.getAssertionMode();
        return mode;
    }

    private String generateUUID() throws Exception {

        UUID idOne = UUID.randomUUID();
        logger.info("UUID: " + idOne);
        String sid = idOne.toString();
        return sid.replace("-", "");
    }

}
