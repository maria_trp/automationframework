package commonPackage.tools;

import java.io.IOException;
import java.util.Map;

import static commonPackage.tools.GetSystemParameters.getInputDataJson;

public class DataMapping<T> {

    private String Jsonfile;
    private Map<String, T> data;
    private final Class<T> type;

    public DataMapping(Class<T> type) {
        this.type = type;
    }

    public T getData(String key) throws IOException {

        if (data == null) {
            Jsonfile = getInputDataJson();
            data = JsonParser.readFile(Jsonfile, type);
        }

        if (data != null) {
            return data.get(key);
        }
        return null;
    }
}
