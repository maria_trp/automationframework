package commonPackage.tools;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class JsonParser {

    private static ObjectMapper objectMapper = new ObjectMapper()
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public static ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public static <T> T read(String jsonfile, Class<T> type) {

        try (InputStream in = JsonParser.class.getClassLoader().getResourceAsStream(jsonfile)) {

            return objectMapper.readValue(in, type);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static <T> T read(String jsonfile, TypeReference<T> type) {

        try (InputStream in = JsonParser.class.getClassLoader().getResourceAsStream(jsonfile)) {

            return objectMapper.readValue(in, type);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static <T> T readFile(String jsonfile, TypeReference<T> type) {
        try (FileInputStream f = new FileInputStream(jsonfile)) {
            return objectMapper.readValue(f, type);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> Map<String, T> readFile(String jsonfile, Class<T> type) {
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try (InputStream f= JsonParser.class.getResourceAsStream(jsonfile)) {
            TypeFactory typeFactory = objectMapper.getTypeFactory();
            JavaType inner = typeFactory.constructParametricType(Map.class, String.class, type);
            return objectMapper.readValue(f, inner);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String mapObjectToString(Object o) throws JsonProcessingException {
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper.writeValueAsString(o);
    }

    public static <T> T mapStringToObject(String text, Class<T> valueType) throws IOException {
        return objectMapper.readValue(text, valueType);
    }


}
