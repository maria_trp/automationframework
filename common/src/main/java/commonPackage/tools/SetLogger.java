package commonPackage.tools;

import org.apache.log4j.Logger;

import static commonPackage.tools.GetSystemParameters.getSystemFileProperty;

public class SetLogger {

    public SetLogger() {

    }

    public static Logger setLoggerLevel(Logger logger) {
        switch (getSystemFileProperty("LOGGER_LEVEL")) {
            case "DEBUG":
                logger.setLevel(org.apache.log4j.Level.DEBUG);
                break;
            case "ERROR":
                logger.setLevel(org.apache.log4j.Level.ERROR);
                break;
            case "FATAL":
                logger.setLevel(org.apache.log4j.Level.FATAL);
                break;
            case "INFO":
                logger.setLevel(org.apache.log4j.Level.INFO);
                break;
            case "OFF":
                logger.setLevel(org.apache.log4j.Level.OFF);
                break;
            case "TRACE":
                logger.setLevel(org.apache.log4j.Level.TRACE);
                break;
            case "WARN":
                logger.setLevel(org.apache.log4j.Level.WARN);
                break;
            default:
                logger.setLevel(org.apache.log4j.Level.ALL);
                break;

        }
        return logger;
    }
}
