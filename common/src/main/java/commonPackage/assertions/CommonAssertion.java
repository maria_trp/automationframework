package commonPackage.assertions;

import commonPackage.pages.PageBase;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class CommonAssertion extends PageBase {

    public CommonAssertion(WebDriver driver) throws Exception {
        super(driver);
    }

    public static void assertText(String actualValue, String expectedValue) throws Exception {
        Assert.assertEquals(expectedValue, actualValue);

    }

    public static void assertContains(String actualValue, String expectedValue) throws Exception {
        Assert.assertThat(actualValue, CoreMatchers.containsString(expectedValue));

    }

    private void assertMandatoryField(String detailIdentifier) throws Exception {
        String actualValue = getMessage(detailIdentifier);
        CommonAssertion.assertContains(actualValue, "Field is required");
    }

}
