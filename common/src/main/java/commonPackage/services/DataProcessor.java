package commonPackage.services;

public interface DataProcessor {

    void preRunUpdate() throws Exception;

    void postRunUpdate() throws Exception;
}
