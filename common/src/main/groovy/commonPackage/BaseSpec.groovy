package commonPackage

import commonPackage.assertions.CommonAssertion
import commonPackage.pages.LoginPage
import commonPackage.tools.TimingsManager
import io.github.bonigarcia.wdm.WebDriverManager
import org.apache.log4j.Logger
import org.openqa.selenium.UnexpectedAlertBehaviour
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxBinary
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.firefox.FirefoxProfile
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.LocalFileDetector
import org.openqa.selenium.remote.RemoteWebDriver
import spock.lang.Retry
import spock.lang.Shared
import spock.lang.Specification

import java.util.concurrent.TimeUnit

import static commonPackage.tools.GetSystemParameters.*
@Retry(count = 1)
class BaseSpec extends Specification {

    static Logger logger = Logger.getLogger(BaseSpec.class)

    @Shared
    WebDriver driver

    static LoginPage loginPage

    static TimingsManager timings = new TimingsManager()
    static CommonAssertion commonAssertion

    //Web driver opens before every suite
    def setupSpec() {
        try {
            initWebDriver()
        } catch (IOException ex) {
            logger.info(ex)
        }
        commonAssertion = new CommonAssertion(driver)
    }

    def cleanupSpec() {
        timings.starttime = 0L
        driver.close()
        driver.quit()
    }

    void loadLink(String applicationUrl) {
        String baseUrl = getConfFileProperty(applicationUrl)
        String logoutUrl = getConfFileProperty("LOGOUT_URL")
        driver.navigate().to(logoutUrl)
        driver.navigate().to(baseUrl)
    }

    void loadLinkWithoutLoggingOut(String applicationUrl) {
        String baseUrl = getConfFileProperty(applicationUrl)
        driver.navigate().to(baseUrl)
    }

    void initWebDriver() throws FileNotFoundException, IOException {
        String driverType = getBrowserDriver()

        // CHROME
        if (driverType.compareTo("Chrome") == 0) {
            //WebDriverManager.chromedriver().forceDownload()
            WebDriverManager.chromedriver().targetPath("src/main/resources/Drivers/chromedriver/").setup()
            ChromeOptions chromeoptions = new ChromeOptions()
            chromeoptions.setCapability("chrome.switches", Arrays.asList("--ignore-certificate-errors"))
            chromeoptions.setAcceptInsecureCerts(true)
            //chromeoptions.addArguments("--start-maximized")

            //chrome options to run headless browser on background
            //chromeoptions.addArguments("--no-sandbox")
            //chromeoptions.addArguments("--headless")
            //chromeoptions.addArguments("disable-gpu")
            chromeoptions.addArguments("window-size=1920,1080")

            driver = new ChromeDriver(chromeoptions)
            driver.manage().deleteAllCookies()
        }

        // FIREFOX
        if (driverType.compareTo("Firefox") == 0) {

            WebDriverManager.firefoxdriver().targetPath("src/main/resources/Drivers/firefoxdriver/").setup()
            FirefoxOptions options = new FirefoxOptions()
            File pathToBinary = new File("C:\\firefox-sdk\\bin\\firefox.exe")
            FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary)
            options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true)
            options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE)
            options.setCapability(FirefoxDriver.MARIONETTE, false)
            FirefoxProfile profile = new FirefoxProfile()
            profile.setAcceptUntrustedCertificates(true)
            profile.setPreference("dom.file.createInChild", true)
            options.setProfile(profile).setBinary(ffBinary)
            driver = new FirefoxDriver(options)
        }

        // REMOTE DRIVER
        if (driverType.compareTo("Remote") == 0) {
            DesiredCapabilities capability = DesiredCapabilities.chrome()
            capability.setCapability("build", "JUnit-Parallel")
            try {
                driver = new RemoteWebDriver(new URL("http://192.168.7.75:4446/wd/hub"), capability)

            } catch (MalformedURLException e) {
                throw new RuntimeException(e)
            }
            ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector())
        }

        // SAUCE LAB
        if (driverType.compareTo("Sauce") == 0) {
            DesiredCapabilities capability = new DesiredCapabilities()
            capability.setCapability("username", getUserName())
            capability.setCapability("accessKey", getAccessKey())
            capability.setCapability(CapabilityType.BROWSER_NAME, getBrowser())
            capability.setCapability(CapabilityType.VERSION, getVersion())
            capability.setCapability("platform", getOs())
            capability.setCapability("extendedDebugging", true)
            capability.setCapability("seleniumVersion", "3.13.0")
            final String URL = getUrl()
            try {
                driver = new RemoteWebDriver(new URL(URL), capability)
            } catch (MalformedURLException e) {
                throw new RuntimeException(e)
            }
        }

        driver.manage().window().maximize()
        driver.manage().timeouts().pageLoadTimeout(180, TimeUnit.SECONDS)
    }

    LoginPage navigateToLoginPage() {

        loadLink("PMS_URL")
        loginPage = new LoginPage(driver, timings)
        return loginPage
    }

    void navigateToPMSLoginPage() {

        loadLinkWithoutLoggingOut("PMS_URL")
    }
}
