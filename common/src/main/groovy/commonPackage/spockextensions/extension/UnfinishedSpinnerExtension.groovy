package commonPackage.spockextensions.extension

import commonPackage.spockextensions.listener.UnfinishedSpinnerListener
import org.spockframework.runtime.extension.IGlobalExtension
import org.spockframework.runtime.model.SpecInfo

class UnfinishedSpinnerExtension implements IGlobalExtension {

    @Override
    void start() {}

    @Override
    void stop() {}

    @Override
    void visitSpec(SpecInfo spec) {
        UnfinishedSpinnerListener unfinishedSpinnerListener = new UnfinishedSpinnerListener()
        spec.addListener(unfinishedSpinnerListener)
    }
}
