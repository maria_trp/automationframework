package commonPackage.spockextensions.extension


import commonPackage.spockextensions.listener.SeleniumScreenshotListener
import org.openqa.selenium.WebDriver
import org.spockframework.runtime.extension.IGlobalExtension
import org.spockframework.runtime.extension.IMethodInterceptor
import org.spockframework.runtime.extension.IMethodInvocation
import org.spockframework.runtime.model.SpecInfo

class ScreenshotExtension implements IGlobalExtension {

    @Override
    void start() {}

    @Override
    void stop() {}

    @Override
    void visitSpec(SpecInfo spec) {
        SeleniumScreenshotListener logSpockErrorListener = new SeleniumScreenshotListener()

        IMethodInterceptor screenshotInterceptor = new IMethodInterceptor() {
            @Override
            void intercept(IMethodInvocation invocation) {
                logSpockErrorListener.browser = invocation.sharedInstance.properties["driver"] as WebDriver
                invocation.proceed()
            }
        }

        spec.allFixtureMethods*.addInterceptor(screenshotInterceptor)
        spec.allFeatures*.addInterceptor(screenshotInterceptor)

        spec.addListener(logSpockErrorListener)
    }
}