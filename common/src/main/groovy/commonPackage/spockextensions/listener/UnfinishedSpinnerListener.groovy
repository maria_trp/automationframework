package commonPackage.spockextensions.listener

import commonPackage.exceptions.LoadingSpinnerException
import org.openqa.selenium.TimeoutException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.spockframework.runtime.AbstractRunListener
import org.spockframework.runtime.model.ErrorInfo

class UnfinishedSpinnerListener extends AbstractRunListener {

    private static final Logger LOG = LoggerFactory.getLogger(UnfinishedSpinnerListener)

    void error(ErrorInfo error) {
        if (!(error.getException() instanceof TimeoutException)) {
            return
        }
        if (isSpinnerTimeout(error.getException().message)) {
            LOG.error("Spinner has not finished loading...")
            throw new LoadingSpinnerException("Spinner has not finished loading...")
        }
    }

    private static Boolean isSpinnerTimeout(String errorMessage) {
        return errorMessage.contains("@id,'spinner'") ||
                errorMessage.contains("@class,'spinner'") ||
                errorMessage.contains("@class='spinner") ||
                errorMessage.contains("@class='loadingIndicator-disable'")
    }
}
