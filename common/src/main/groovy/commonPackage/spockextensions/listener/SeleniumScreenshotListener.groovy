package commonPackage.spockextensions.listener


import org.apache.commons.io.FileUtils
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver
import org.spockframework.runtime.AbstractRunListener
import org.spockframework.runtime.model.ErrorInfo

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate

import static commonPackage.tools.GetSystemParameters.getBrowserDriver
import static commonPackage.tools.GetSystemParameters.getVersion

class SeleniumScreenshotListener extends AbstractRunListener {

    WebDriver browser
    private String today = LocalDate.now().toString()
    protected String screenshotLocation = "reports/spock-screenshots"

    SeleniumScreenshotListener() {

    }

    void error(ErrorInfo error) {
        if(error.method.parent.name.contains("GridParallelOneFlowFromPropertiesSpec")){
            return
        }

        TakesScreenshot takesScreenshot = (TakesScreenshot) browser

        String testMethodName = "${error.method.parent.name}-${error.method.name}"
        if(testMethodName == null) {
            return
        }
        File scrFile = takesScreenshot.getScreenshotAs(OutputType.FILE)
        File destFile = getScreenshotDestinationFile(testMethodName)

        File srcPageSourceFile = capturePageSourceCode()
        File destPageSourceFile = getPageSourceDestinationFile(testMethodName)

        try {
            FileUtils.copyFile(scrFile, destFile)
            FileUtils.copyFile(srcPageSourceFile, destPageSourceFile)
        } catch (IOException ioe) {
            throw new RuntimeException(ioe)
        } catch (Exception e1) {
            e1.printStackTrace()
        }
    }

    String createFileNameWithoutExtension(String description) {
        String userDirectory = screenshotLocation
        String date = getDateTime()
        String fileName = description + "_" + date
        // add date of today
        String dateForDir = today
        String absoluteFileName = userDirectory + "/" + dateForDir + "/" + fileName
        return absoluteFileName
    }

    File capturePageSourceCode() {
        File srcPageSourceFile = File.createTempFile("pageSrc", ".html")
        srcPageSourceFile.write(browser.getPageSource())
        return srcPageSourceFile
    }

    File getScreenshotDestinationFile(String description) {
        String absoluteFileName = createFileNameWithoutExtension(description) + ".png"
        return new File(absoluteFileName)
    }

    File getPageSourceDestinationFile(String description) {
        String absoluteFileName = createFileNameWithoutExtension(description) + ".html"
        return new File(absoluteFileName)
    }

    String getDateTime() {
        Date date = Calendar.getInstance().getTime()

        // Display a date in day, month, year format
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy_HH_mm")
        String today = formatter.format(date)
        return today
    }

    private String isSauceLab() throws Exception {
        String mode = getBrowserDriver()
        return mode

    }

    private String buildInfo() throws Exception {
        String buildId = getVersion()
        return buildId
    }
}
