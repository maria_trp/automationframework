#Automated UI Tests

Automated UI tests for each module<br>
**Technology stack:**
* Selenium
* Java 8
* Groovy (http://groovy-lang.org/documentation.html)
* Spock Framework (http://spockframework.org/spock/docs/1.3/index.html)

##Test implementation
*   Tests are written in **Spock Framework** with **Groovy**
*   Utility classes are written in **Java 8** and are placed in each module's `src/main/java`
*   **Page Object** design pattern is followed
*   There is the `common` module when common utilities are implemented
*   Each Spock test (specification) should be Groovy class that ends with `*Spec.groovy` and should extend its module's base `*Spec.groovy` class.
*   Each module's page class `*Page.java` should extend the module's `*BasePage.java` class (except for some cases).
*   Each test should have distinct **Spock labels** `given, when, and, then` along with a description for behavioral reports.

##Build
Under parent module `testing/` run:<br>
`mvn clean install -Pxtest`<br>
This command will compile utility classes and tests, without running them (profile: <b>xtest</b>)

##Test execution
* #####Manual Process:
   *    Import as maven project in your IDE.
   *    In preferred  module's `src/main/resources/system.properties` uncomment the `environment.name=<ENV>` of the desired environment.
   *    In preferred  module's `src/main/resources/system.properties` uncomment the `customer.name=<CUSTOMER>` of the desired environment.<br>
        `customer.name=CORE` is for internal environment <br>
   *    Run the whichever test under modules `src/test/groovy/..../specifications/` with right-click.
   *    Test reports are under module's `target/spock-reports`
   *    Screenshot and DOM of failed tests are under module's `target/spock-screenshots`
* #####Command line Process:
    *   #####Build verification
           `mvn clean -Psanity -Denvironment.name=<ENV> -Dcustomer.name=<CUSTOMER> test -pl <module_name>`
    *   #####All Tests
           `mvn clean -Palltests -Denvironment.name=<ENV> -Dcustomer.name=<CUSTOMER> test -pl <module_name>`
           

##Test environment properties
Each module holds its own test-environment related data.<br>
Target environment is declared via `environment.name` property inside module's: `src/main/resources/system.properties`<br>
For each supported environment, its properties are declared into module's: <br>
`src/main/resources/propertiesFile/CORE_<ENV_NAME>_conf.properties`<br>

##Test data files
Test data files are located inside module's: `src/main/resources/dataJson/<CUSTOMER>_<ENV_TYPE>_InputData.json`
* <b>CUSTOMER: CORE</b> and <b>ENV_TYPE: INTERNAL</b>

##Execute automation tests with JAR files from CMD
- In project folder execute
  mvn clean install
- In target folder of each module you have to execute following command
  java -jar PMS-0.0.1-SNAPSHOT.jar BUILD CORE SANITY
  With the above command we will execute all tests from SanitySpec with CORE data.
  You may change all arguments in order to choose environment, data and test suite.
  Available test suites are SANITY and REGRESSION.
